import 'package:devocionario2/constantes.dart';
import 'package:devocionario2/firebase_options.dart';
import 'package:devocionario2/pages/0-login/bloc/bloc_login.dart';
import 'package:devocionario2/pages/0-login/page_login.dart';
import 'package:devocionario2/pages/1-register/bloc/bloc_register.dart';
import 'package:devocionario2/pages/1-register/page_register.dart';
import 'package:devocionario2/pages/2-seleccionador_devocion/page_seleccionador_devocion.dart';
import 'package:devocionario2/pages/3-acceso_a_las_promesas/bloc/bloc_acceso_a_las_promesas.dart';
import 'package:devocionario2/pages/3-acceso_a_las_promesas/page_acceso_a_las_promesas.dart';
import 'package:devocionario2/pages/4-oraciones/bloc/bloc_oraciones.dart';
import 'package:devocionario2/pages/4-oraciones/page_oraciones.dart';
import 'package:devocionario2/pages/5-finalizacion/bloc/bloc_finalizacion.dart';
import 'package:devocionario2/pages/5-finalizacion/page_finalizacion.dart';
import 'package:devocionario2/pages/6-progreso/bloc/bloc_progreso.dart';
import 'package:devocionario2/pages/6-progreso/page_progreso.dart';
import 'package:devocionario2/pages/7-promesas/bloc/bloc_promesas.dart';
import 'package:devocionario2/pages/7-promesas/page_promesas.dart';
import 'package:devocionario2/pages/8-profile/bloc/bloc_profile.dart';
import 'package:devocionario2/pages/8-profile/page_profile.dart';
import 'package:devocionario2/pages/9-configuracion/bloc/bloc_configuracion.dart';
import 'package:devocionario2/pages/9-configuracion/page_configuracion.dart';
import 'package:devocionario2/pages/11-ver_progreso/page_ver_progreso.dart';
import 'package:devocionario2/services/auth.dart';
import 'package:devocionario2/services/getters.dart';
import 'package:devocionario2/widgets_global/bloc_drawer/bloc_drawer.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    final fuenteGeneral = GoogleFonts.dmSans();

    return MultiBlocProvider(
      providers: [
        BlocProvider<BlocLogin>(
          create: (context) => BlocLogin(),
        ),
        BlocProvider<BlocRegister>(
          create: (context) => BlocRegister(),
        ),
        BlocProvider<BlocAccesoPromesas>(
          create: (context) => BlocAccesoPromesas(),
        ),
        BlocProvider<BlocProgreso>(
          create: (context) => BlocProgreso(),
        ),
        BlocProvider<BlocOraciones>(
          create: (context) => BlocOraciones(),
        ),
        BlocProvider<BlocProgreso>(
          create: (context) => BlocProgreso(),
        ),
        BlocProvider<BlocPromesas>(
          create: (context) => BlocPromesas(),
        ),
        BlocProvider<BlocDrawer>(
          create: (context) => BlocDrawer(),
        ),
        BlocProvider<BlocFinalizacion>(
          create: (context) => BlocFinalizacion(),
        ),
        BlocProvider<BlocProfile>(
          create: (context) => BlocProfile(),
        ),
        BlocProvider<BlocConfiguracion>(
          create: (context) => BlocConfiguracion(),
        ),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          fontFamily: fuenteGeneral.fontFamily,
        ),
        home: StreamBuilder<User?>(
          stream: Auth().authStateChanges,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              if (snapshot.connectionState == ConnectionState.waiting) {
                return const Scaffold(
                  body: Center(
                    child: CircularProgressIndicator(
                      color: marronmedio,
                    ),
                  ),
                );
              } else {
                return FutureBuilder<Widget>(
                  future: devolverPaginaActual(),
                  builder: (BuildContext context,
                      AsyncSnapshot<Widget> widgetSnapshot) {
                    if (widgetSnapshot.connectionState ==
                        ConnectionState.waiting) {
                      return const Center(
                        child: Scaffold(
                          body: Center(
                            child: CircularProgressIndicator(
                              color: marronmedio,
                              backgroundColor: Color(0xffdddddd),
                            ),
                          ),
                        ),
                      );
                    }
                    if (widgetSnapshot.hasData) {
                      return widgetSnapshot.data!;
                    }
                    if (widgetSnapshot.hasError) {
                      // Handle error
                      return Container();
                    }
                    // Return default widget while waiting for database response
                    return const Scaffold(
                      body: Center(
                        child: CircularProgressIndicator(
                          color: marronmedio,
                        ),
                      ),
                    );
                  },
                );
              }
            } else {
              return const PageLogin();
            }
          },
        ),
      ),
    );
  }
}

Future<Widget> devolverPaginaActual() async {
  var asd = await getFieldValue(colection: 'devociones', campo: 'paginaActual');
  switch (asd) {
    case 0:
      return const PageLogin();
    case 1:
      return const PageRegister();
    case 2:
      return const PageSeleccionadorDevocion();
    case 3:
      return const PageAccesoPromesas();
    case 4:
      return const PageOraciones();
    case 5:
      return const PageFinalizacion();
    case 6:
      return const PageProgreso();
    case 7:
      return const PagePromesas();
    case 8:
      return const PageProfile();
    case 9:
      return const PageConfiguracion();
    case 11:
      return const PageVerProgreso();
    default:
      return const PageOraciones();
  }
}

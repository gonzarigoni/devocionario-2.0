// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:equatable/equatable.dart';

abstract class BlocDrawerEvent extends Equatable {
  const BlocDrawerEvent();

  @override
  List<Object> get props => [];
}

class BlocDrawerGetUserDataEvent extends BlocDrawerEvent {
  const BlocDrawerGetUserDataEvent({
    required this.nombre,
    required this.apellido,
  });
  final String? nombre;
  final String? apellido;
}

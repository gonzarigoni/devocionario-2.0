import 'dart:async';
import 'package:devocionario2/services/getters.dart';
import 'package:devocionario2/widgets_global/bloc_drawer/bloc_drawer_event.dart';
import 'package:devocionario2/widgets_global/bloc_drawer/bloc_drawer_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class BlocDrawer extends Bloc<BlocDrawerEvent, BlocDrawerState> {
  BlocDrawer()
      : super(const DrawerInitial(
          nombre: '',
          apellido: '',
        )) {
    on<BlocDrawerGetUserDataEvent>(_getUserData);
  }

  FutureOr<void> _getUserData(
      BlocDrawerGetUserDataEvent event, Emitter<BlocDrawerState> emit) async {
    try {
      Map<String, dynamic> response = await getUserDataDrawer();
      final String? nombre = response['name'];
      final String? apellido = response['lastname'];
      emit(
        state.copyWith(
          nombre: nombre,
          apellido: apellido,
        ),
      );
    } catch (e) {
      print(e);
    }
  }
}

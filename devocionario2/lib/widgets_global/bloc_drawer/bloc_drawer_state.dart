// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:equatable/equatable.dart';

class BlocDrawerState extends Equatable {
  const BlocDrawerState({
    required this.nombre,
    required this.apellido,
  });

  final String nombre;
  final String apellido;
  @override
  List<Object> get props => [
        nombre,
        apellido,
      ];

  BlocDrawerState copyWith({
    final String? nombre,
    final String? apellido,
  }) {
    return BlocDrawerState(
      nombre: nombre ?? this.nombre,
      apellido: apellido ?? this.apellido,
    );
  }
}

class DrawerInitial extends BlocDrawerState {
  const DrawerInitial({
    required String nombre,
    required String apellido,
  }) : super(
          nombre: nombre,
          apellido: apellido,
        );
}

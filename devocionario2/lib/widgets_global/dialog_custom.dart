// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:devocionario2/constantes.dart';
import 'package:flutter/material.dart';
import 'package:full_responsive/full_responsive.dart';

class ShowAlertCustom extends StatelessWidget {
  const ShowAlertCustom({
    Key? key,
    required this.texto,
    required this.onTap,
  }) : super(key: key);
  final String texto;
  final VoidCallback onTap;
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15),
      ),
      contentPadding: EdgeInsets.zero,
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            decoration: const BoxDecoration(
              borderRadius: BorderRadius.only(
                topRight: Radius.circular(15),
                topLeft: Radius.circular(15),
              ),
              color: marronclarito,
            ),
            height: 50,
            width: MediaQuery.of(context).size.width,
            child: Padding(
              padding: const EdgeInsets.all(8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Row(
                    children: [
                      Icon(
                        Icons.info,
                        color: blanquito,
                      ),
                      Text(
                        ' Atención',
                        style: TextStyle(color: Colors.white),
                      ),
                    ],
                  ),
                  TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: const Icon(
                      Icons.close,
                      color: blanquito,
                    ),
                  )
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(15, 15, 15, 16),
            child: Text(
              texto,
              style: const TextStyle(color: marronmedio, fontSize: 14),
              textAlign: TextAlign.center,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(bottom: 8),
            child: SizedBox(
              height: 30,
              width: 115,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  backgroundColor: enabledColor,
                ),
                onPressed: onTap,
                child: Text(
                  'Continuar',
                  style: TextStyle(
                    fontSize: 17.pf,
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}

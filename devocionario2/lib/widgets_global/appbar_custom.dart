import 'package:devocionario2/constantes.dart';
import 'package:devocionario2/pages/7-promesas/page_promesas.dart';
import 'package:devocionario2/services/getters.dart';
import 'package:devocionario2/services/setters.dart';
import 'package:flutter/material.dart';
import 'package:full_responsive/full_responsive.dart';

class AppBarCustom extends StatelessWidget implements PreferredSizeWidget {
  const AppBarCustom({
    Key? key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return AppBar(
      iconTheme: const IconThemeData(color: marronclarito),
      elevation: 0,
      centerTitle: true,
      title: Text(
        'Oración',
        style: TextStyle(
          fontWeight: FontWeight.w700,
          fontSize: 20.pf,
          color: marronmedio,
        ),
      ),
      backgroundColor: Colors.transparent,
      actions: [
        GestureDetector(
          onTap: () async {
            updatePaginaAnterior(
                newValue: await getFieldValue(
                    campo: 'paginaActual', colection: 'devociones'));
            Navigator.of(context).pushReplacement(
                MaterialPageRoute(builder: (BuildContext context) {
              return const PagePromesas();
            }));
          },
          child: const Padding(
            padding: EdgeInsets.only(right: 10.0),
            child: Icon(
              Icons.feed_outlined,
              color: marronclarito,
              size: 24,
            ),
          ),
        ),
      ],
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}

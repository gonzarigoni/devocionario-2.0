import 'package:devocionario2/constantes.dart';
import 'package:devocionario2/pages/11-ver_progreso/page_ver_progreso.dart';
import 'package:devocionario2/pages/4-oraciones/page_oraciones.dart';
import 'package:devocionario2/pages/8-profile/page_profile.dart';
import 'package:devocionario2/pages/9-configuracion/page_configuracion.dart';
import 'package:devocionario2/services/auth.dart';
import 'package:devocionario2/widgets_global/bloc_drawer/bloc_drawer.dart';
import 'package:devocionario2/widgets_global/bloc_drawer/bloc_drawer_event.dart';
import 'package:devocionario2/widgets_global/bloc_drawer/bloc_drawer_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:full_responsive/full_responsive.dart';

class DrawerCustom extends StatefulWidget {
  const DrawerCustom({
    super.key,
  });

  @override
  State<DrawerCustom> createState() => _DrawerCustomState();
}

class _DrawerCustomState extends State<DrawerCustom> {
  @override
  void initState() {
    context
        .read<BlocDrawer>()
        .add(const BlocDrawerGetUserDataEvent(nombre: '', apellido: ''));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      backgroundColor: marronmedio,
      child: Padding(
        padding: const EdgeInsets.all(16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                GestureDetector(
                  onTap: () => Navigator.of(context).pop(),
                  child: const Icon(
                    Icons.close,
                    color: blanquito,
                  ),
                ),
                EspacioVertical(height: 15.ph),
                Row(
                  children: [
                    Container(
                      alignment: Alignment.center,
                      child: Container(
                        width: 56.pw,
                        height: 56.pw,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(28.0),
                        ),
                        child: ClipOval(
                          child: FadeInImage.assetNetwork(
                              placeholder:
                                  'assets/images/placeholder_image.png',
                              image:
                                  'https://firebasestorage.googleapis.com/v0/b/fluttertest-a56bb.appspot.com/o/imagen0.jpg?alt=media&token=90f2529a-0d2a-443a-bbfc-13f357c39958',
                              fit: BoxFit.cover),
                        ),
                      ),
                    ),

                    // SizedBox(
                    //     width: 56.pw,
                    //     child: const CircleAvatar(
                    //       radius: 28,
                    //       backgroundColor: blanquito,
                    //       backgroundImage: NetworkImage(
                    //           'https://firebasestorage.googleapis.com/v0/b/fluttertest-a56bb.appspot.com/o/imagen0.jpg?alt=media&token=90f2529a-0d2a-443a-bbfc-13f357c39958'),
                    //     )
                    //     ),
                    BlocBuilder<BlocDrawer, BlocDrawerState>(
                      builder: (context, state) {
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 8.pw),
                              child: Text(
                                state.nombre,
                                style: const TextStyle(
                                  color: blanquito,
                                  fontWeight: FontWeight.w700,
                                  fontSize: 18,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ),
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 8.pw),
                              child: Text(
                                state.apellido,
                                style: const TextStyle(
                                  color: blanquito,
                                  fontWeight: FontWeight.w700,
                                  fontSize: 18,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          ],
                        );
                      },
                    ),
                  ],
                ),
                EspacioVertical(height: 60.ph),
                Padding(
                  padding: const EdgeInsets.only(bottom: 16.0),
                  child: TextButton.icon(
                    onPressed: () {
                      Navigator.of(context)
                          .pushReplacement(MaterialPageRoute<PageProfile>(
                        builder: (BuildContext context) {
                          return const PageProfile();
                        },
                      ));
                    },
                    icon: SvgPicture.asset(
                      'assets/images/Person.svg',
                      width: 18,
                      height: 18,
                    ),
                    label: const Text(
                      'Perfil',
                      style: TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: 18,
                        color: blanquito,
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 16.0),
                  child: TextButton.icon(
                    onPressed: () {
                      Navigator.of(context)
                          .pushReplacement(MaterialPageRoute<PageOraciones>(
                        builder: (BuildContext context) {
                          return const PageOraciones();
                        },
                      ));
                    },
                    icon: SvgPicture.asset(
                      'assets/images/Cross.svg',
                      width: 18,
                      height: 18,
                    ),
                    label: const Text(
                      'Mis oraciones',
                      style: TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: 18,
                        color: blanquito,
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 16.0),
                  child: TextButton.icon(
                    onPressed: () {
                      Navigator.of(context)
                          .pushReplacement(MaterialPageRoute<PageVerProgreso>(
                        builder: (BuildContext context) {
                          return const PageVerProgreso();
                        },
                      ));
                    },
                    icon: const Icon(
                      Icons.show_chart_rounded,
                      // Icons.fast_forward_outlined,
                      color: blanquito,
                    ),
                    label: const Text(
                      'Ver progreso',
                      style: TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: 18,
                        color: blanquito,
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 16.0),
                  child: TextButton.icon(
                    onPressed: () {
                      Navigator.of(context)
                          .pushReplacement(MaterialPageRoute<PageConfiguracion>(
                        builder: (BuildContext context) {
                          return const PageConfiguracion();
                        },
                      ));
                    },
                    icon: SvgPicture.asset(
                      'assets/images/Settings.svg',
                      width: 18,
                      height: 18,
                    ),
                    label: const Text(
                      'Configuración',
                      style: TextStyle(
                        fontWeight: FontWeight.w700,
                        fontSize: 18,
                        color: blanquito,
                      ),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width,
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    TextButton.icon(
                      onPressed: () {
                        signOut();
                      },
                      icon: const Icon(
                        Icons.output_outlined,
                        color: blanquito,
                      ),
                      label: const Text(
                        'Cerrar Sesión',
                        style: TextStyle(
                          fontWeight: FontWeight.w700,
                          fontSize: 18,
                          color: blanquito,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: Row(
                        children: [
                          const Text(
                            'Powered by ',
                            style: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 14,
                              color: blanquito,
                            ),
                          ),
                          SvgPicture.asset(
                            'assets/images/nidus_logo.svg',
                            width: 18,
                            height: 18,

                            // ignore: deprecated_member_use
                            color: blanquito,
                          ),
                          const Text(
                            'Nidus®',
                            style: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 14,
                              color: blanquito,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ]),
            ),
          ],
        ),
      ),
    );
  }
}

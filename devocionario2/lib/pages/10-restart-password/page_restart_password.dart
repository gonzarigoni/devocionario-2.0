import 'package:auto_route/auto_route.dart';
import 'package:devocionario2/constantes.dart';
import 'package:devocionario2/widgets_global/dialog_custom.dart';
import 'package:email_validator/email_validator.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:full_responsive/full_responsive.dart';

@RoutePage()
class PageRestartPassword extends StatelessWidget {
  PageRestartPassword({super.key});

  final TextEditingController controllerReset = TextEditingController();

  Future resetPassword() async {
    FirebaseAuth.instance.sendPasswordResetEmail(email: controllerReset.text);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        toolbarHeight: 40.ph,
        backgroundColor: Colors.transparent,
        leading: GestureDetector(
          onTap: () {
            Navigator.of(context).pop();
          },
          child: const Icon(
            Icons.arrow_back,
            color: marronclarito,
          ),
        ),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Column(
              children: [
                Text(
                  '¡Bienvenido!',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontWeight: FontWeight.w700,
                    fontSize: 40.pf,
                    color: marronmedio,
                  ),
                ),
                EspacioVertical(
                  height: 20.ph,
                ),
                Text(
                  'Solicita un cambio de contraseña.',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontWeight: FontWeight.w500,
                    fontSize: 20.pf,
                    color: marronoscuro,
                  ),
                ),
              ],
            ),
            EspacioVertical(height: 60.ph),
            SizedBox(
              width: 300.pw,
              child: TextField(
                style: const TextStyle(
                  color: marronmedio,
                ),
                controller: controllerReset,
                decoration: const InputDecoration(
                  focusedBorder: UnderlineInputBorder(
                    borderSide: BorderSide(
                      color: fontTextField,
                    ),
                  ),
                  enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(
                      color: negro,
                    ),
                  ),
                  labelText: 'Ingrese Email:',
                  labelStyle: TextStyle(
                    color: fontTextField,
                  ),
                ),
              ),
            ),
            EspacioVertical(height: 40.ph),
            Container(
              width: 250.pw,
              height: 30.ph,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
              ),
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  backgroundColor: marronclarito,
                ),
                onPressed: () async {
                  if (EmailValidator.validate(controllerReset.text)) {
                    try {
                      resetPassword();
                    } catch (e) {
                      showDialog(
                        context: context,
                        builder: (context) => ShowAlertCustom(
                          texto: 'Error al enviar el correo electrónico',
                          onTap: () async {
                            Navigator.of(context).pop();
                          },
                        ),
                      );
                    }
                  } else {
                    showDialog(
                      context: context,
                      builder: (context) => ShowAlertCustom(
                        texto: 'Correo electrónico no valido',
                        onTap: () async {
                          Navigator.of(context).pop();
                        },
                      ),
                    );
                  }
                },
                child: Text(
                  'Restablecer contraseña',
                  style: TextStyle(
                    fontWeight: FontWeight.w700,
                    fontSize: 17.pf,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

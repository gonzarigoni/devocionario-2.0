// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:equatable/equatable.dart';

abstract class BlocProfileEvent extends Equatable {
  const BlocProfileEvent();

  @override
  List<Object> get props => [];
}

class BlocProfileGetUserDataEvent extends BlocProfileEvent {
  const BlocProfileGetUserDataEvent({
    required this.nombre,
    required this.apellido,
    required this.email,
  });
  final String? nombre;
  final String? apellido;
  final String? email;
}

// class BlocProfileGetImageEvent extends BlocProfileEvent {
//   const BlocProfileGetImageEvent({
//     required this.image,
//   });
//   final String? image;
// }
class BlocProfileChangeEditableEvent extends BlocProfileEvent {
  const BlocProfileChangeEditableEvent({
    required this.nameIsEditable,
    required this.emailIsEditable,
    this.email,
  });
  final bool nameIsEditable;
  final bool emailIsEditable;
  final String? email;
}

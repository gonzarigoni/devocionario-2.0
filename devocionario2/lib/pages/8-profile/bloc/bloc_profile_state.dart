// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:equatable/equatable.dart';

enum BlocProfileStatus {
  /// estado inicial sin nada cargado
  initial,

  /// estado cargado
  success,

  /// estado de error
  error,

  /// estado cargando...
  loading;

  ///devuleve true si el estado es Success
  bool get isSuccess => this == BlocProfileStatus.success;

  ///devuelve true si el estado es Error
  bool get isError => this == BlocProfileStatus.error;

  ///devuleve true si el estado es Loading
  bool get isLoading => this == BlocProfileStatus.loading;

  ///devuelve un estado de true al iniciar
  bool get isInitial => this == BlocProfileStatus.initial;
}

class BlocProfileState extends Equatable {
  const BlocProfileState({
    required this.nombre,
    required this.apellido,
    required this.email,
    // required this.image,
    required this.nameIsEditable,
    required this.emailIsEditable,
    required this.nombreyapellido,
    required this.status,
  });

  final String nombre;
  final String apellido;
  final String email;
  final String nombreyapellido;
  final bool nameIsEditable;
  final bool emailIsEditable;
  final BlocProfileStatus status;
  // final String image;

  @override
  List<Object> get props => [
        apellido,
        nombre,
        email,
        // image,
        nameIsEditable,
        emailIsEditable, nombreyapellido,
        status,
      ];

  BlocProfileState copyWith({
    final String? nombre,
    final String? apellido,
    final String? email,
    // final String? image,
    final bool? nameIsEditable,
    final bool? emailIsEditable,
    final String? nombreyapellido,
    final BlocProfileStatus? status,
  }) {
    return BlocProfileState(
      nombre: nombre ?? this.nombre,
      apellido: apellido ?? this.apellido,
      email: email ?? this.email,
      // image: image ?? this.image,
      nameIsEditable: nameIsEditable ?? this.nameIsEditable,
      emailIsEditable: emailIsEditable ?? this.emailIsEditable,
      nombreyapellido: nombreyapellido ?? this.nombreyapellido,
      status: status ?? this.status,
    );
  }
}

class ProfileInitial extends BlocProfileState {
  const ProfileInitial({
    required String nombre,
    required String apellido,
    required String email,
    // required String image,
    required bool nameIsEditable,
    required bool emailIsEditable,
    required String nombreyapellido,
    required BlocProfileStatus status,
  }) : super(
          nombre: nombre,
          apellido: apellido,
          email: email,
          // image: image,
          nameIsEditable: nameIsEditable,
          emailIsEditable: emailIsEditable,
          nombreyapellido: nombreyapellido,
          status: BlocProfileStatus.initial,
        );
}

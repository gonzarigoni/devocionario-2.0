import 'dart:async';
import 'package:devocionario2/pages/8-profile/bloc/bloc_profile_event.dart';
import 'package:devocionario2/pages/8-profile/bloc/bloc_profile_state.dart';
import 'package:devocionario2/services/getters.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class BlocProfile extends Bloc<BlocProfileEvent, BlocProfileState> {
  BlocProfile()
      : super(const ProfileInitial(
          nombre: '',
          apellido: '',
          email: 'gonzalomiguelrigoni@gmail.com',
          // image: 'placeholder_image.png',
          nameIsEditable: true,
          emailIsEditable: true,
          nombreyapellido: '',
          status: BlocProfileStatus.initial,
        )) {
    on<BlocProfileGetUserDataEvent>(_getUserData);
    on<BlocProfileChangeEditableEvent>(_changeEditable);
  }

  FutureOr<void> _getUserData(
      BlocProfileGetUserDataEvent event, Emitter<BlocProfileState> emit) async {
    emit(state.copyWith(status: BlocProfileStatus.loading));
    try {
      Map<String, dynamic> response = await getUserDataProfile();
      final String? nombre = response['nombre'];
      final String? apellido = response['apellido'];
      final String? email = response['email'];

      emit(
        state.copyWith(
          nombre: nombre,
          apellido: apellido,
          email: email,
          status: BlocProfileStatus.success,
        ),
      );
    } catch (e) {
      emit(state.copyWith(status: BlocProfileStatus.error));
    }
  }

  FutureOr<void> _changeEditable(BlocProfileChangeEditableEvent event,
      Emitter<BlocProfileState> emit) async {
    try {
      final bool nameIsEditable = event.nameIsEditable;
      final bool emailIsEditable = event.emailIsEditable;
      emit(
        state.copyWith(
          nameIsEditable: nameIsEditable,
          emailIsEditable: emailIsEditable,
        ),
      );
    } catch (e) {
      print(e);
    }
  }
}

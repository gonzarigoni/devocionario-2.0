import 'package:auto_route/auto_route.dart';
import 'package:devocionario2/constantes.dart';
import 'package:devocionario2/pages/7-promesas/page_promesas.dart';
import 'package:devocionario2/pages/8-profile/bloc/bloc_profile.dart';
import 'package:devocionario2/pages/8-profile/bloc/bloc_profile_event.dart';
import 'package:devocionario2/pages/8-profile/bloc/bloc_profile_state.dart';
import 'package:devocionario2/services/getters.dart';
import 'package:devocionario2/services/setters.dart';
import 'package:devocionario2/widgets_global/dialog_custom.dart';
import 'package:devocionario2/widgets_global/drawer_custom.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:full_responsive/full_responsive.dart';
import 'package:flutter_gravatar/flutter_gravatar.dart';
import 'dart:math' as math;

@RoutePage()
class PageProfile extends StatefulWidget {
  const PageProfile({super.key});

  @override
  State<PageProfile> createState() => _PageProfileState();
}

class _PageProfileState extends State<PageProfile> {
  @override
  void initState() {
    updatePaginaActual(newValue: 8);
    context.read<BlocProfile>().add(const BlocProfileGetUserDataEvent(
          nombre: '',
          apellido: '',
          email: 'gonzalomiguelrigoni@gmail.com',
        ));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<BlocProfile, BlocProfileState>(
      builder: (context, state) {
        if (state.status == BlocProfileStatus.loading) {
          return const Scaffold(
            body: Center(
                child: CircularProgressIndicator(
              color: marronmedio,
              backgroundColor: Color(0xffdddddd),
            )),
          );
        }
        if (state.status == BlocProfileStatus.success) {
          return Scaffold(
            appBar: AppBar(
              iconTheme: const IconThemeData(color: marronclarito),
              elevation: 0,
              centerTitle: true,
              title: Text(
                '',
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: 20.pf,
                  color: marronmedio,
                ),
              ),
              backgroundColor: Colors.transparent,
              actions: [
                GestureDetector(
                  onTap: () async {
                    updatePaginaAnterior(
                        newValue: await getFieldValue(
                            campo: 'paginaActual', colection: 'devociones'));
                    Navigator.of(context).pushReplacement(
                        MaterialPageRoute(builder: (BuildContext context) {
                      return const PagePromesas();
                    }));
                  },
                  child: const Padding(
                    padding: EdgeInsets.only(right: 10.0),
                    child: Icon(
                      Icons.feed_outlined,
                      color: marronclarito,
                      size: 24,
                    ),
                  ),
                ),
              ],
            ),
            drawer: const DrawerCustom(),
            body: BlocBuilder<BlocProfile, BlocProfileState>(
              builder: (context, state) {
                TextEditingController nameController =
                    TextEditingController(text: state.nombre);
                TextEditingController apellidoController =
                    TextEditingController(text: state.apellido);
                TextEditingController nombreyapellidoController =
                    TextEditingController(
                        text: '${state.nombre} ${state.apellido}');
                TextEditingController emailController =
                    TextEditingController(text: state.email);
                return Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      EspacioVertical(height: 10.ph),
                      Center(
                        child: Column(
                          children: [
                            Stack(
                              children: [
                                ClipOval(
                                  child: Material(
                                    color: Colors.transparent,
                                    child: Ink.image(
                                      image: NetworkImage(Gravatar(state.email)
                                          .imageUrl(size: 200)),
                                      fit: BoxFit.cover,
                                      width: 85,
                                      height: 85,
                                      child: InkWell(
                                        onTap: () {},
                                      ),
                                    ),
                                  ),
                                ),
                                Positioned(
                                  bottom: 0,
                                  right: 4,
                                  child: ClipOval(
                                    child: Container(
                                      padding: const EdgeInsets.all(4),
                                      color: marronmedio,
                                      child: const Icon(
                                        Icons.camera_alt_outlined,
                                        color: Colors.white,
                                        size: 16,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            EspacioVertical(height: 10.ph),
                            state.emailIsEditable
                                ? !state.nameIsEditable
                                    ? Column(
                                        children: [
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              SizedBox(
                                                width: 300.pw,
                                                height: 60.ph,
                                                child: TextFormField(
                                                  maxLines: 1,
                                                  readOnly:
                                                      state.nameIsEditable,
                                                  controller: nameController,
                                                  decoration: InputDecoration(
                                                    contentPadding:
                                                        const EdgeInsets.all(
                                                            16),
                                                    labelStyle: const TextStyle(
                                                      color: marronoscuro,
                                                    ),
                                                    suffixIcon: GestureDetector(
                                                      onTap: () {
                                                        context
                                                            .read<BlocProfile>()
                                                            .add(
                                                                BlocProfileChangeEditableEvent(
                                                              nameIsEditable: !state
                                                                  .nameIsEditable,
                                                              emailIsEditable: state
                                                                  .emailIsEditable,
                                                            ));
                                                      },
                                                      child: Icon(
                                                        Icons.edit,
                                                        color:
                                                            state.nameIsEditable
                                                                ? marronmedio
                                                                : enabledColor,
                                                        size: 20,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              )
                                            ],
                                          ),
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              SizedBox(
                                                width: 300.pw,
                                                height: 60.ph,
                                                child: TextFormField(
                                                  maxLines: 1,
                                                  readOnly:
                                                      state.nameIsEditable,
                                                  controller:
                                                      apellidoController,
                                                  decoration:
                                                      const InputDecoration(
                                                    contentPadding:
                                                        EdgeInsets.all(16),
                                                    labelStyle: TextStyle(
                                                      color: marronoscuro,
                                                    ),
                                                  ),
                                                ),
                                              )
                                            ],
                                          ),
                                        ],
                                      )
                                    : Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          SizedBox(
                                            width: 300.pw,
                                            height: 60.ph,
                                            child: TextFormField(
                                              maxLines: 1,
                                              readOnly: state.nameIsEditable,
                                              controller:
                                                  nombreyapellidoController,
                                              decoration: InputDecoration(
                                                contentPadding:
                                                    const EdgeInsets.all(16),
                                                labelStyle: const TextStyle(
                                                  color: marronoscuro,
                                                ),
                                                suffixIcon: GestureDetector(
                                                  onTap: () {
                                                    context
                                                        .read<BlocProfile>()
                                                        .add(
                                                            BlocProfileChangeEditableEvent(
                                                          nameIsEditable: !state
                                                              .nameIsEditable,
                                                          emailIsEditable: state
                                                              .emailIsEditable,
                                                        ));
                                                  },
                                                  child: Icon(
                                                    Icons.edit,
                                                    color: state.nameIsEditable
                                                        ? marronmedio
                                                        : enabledColor,
                                                    size: 20,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          )
                                        ],
                                      )
                                : Container(),
                            state.nameIsEditable
                                ? Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      SizedBox(
                                        width: 300.pw,
                                        height: 60.ph,
                                        child: TextFormField(
                                          readOnly: state.emailIsEditable,
                                          controller: emailController,
                                          decoration: InputDecoration(
                                            contentPadding:
                                                const EdgeInsets.all(16),
                                            labelStyle: const TextStyle(
                                              color: marronoscuro,
                                            ),
                                            suffixIcon: GestureDetector(
                                              onTap: () {
                                                context.read<BlocProfile>().add(
                                                      BlocProfileChangeEditableEvent(
                                                        nameIsEditable: state
                                                            .nameIsEditable,
                                                        emailIsEditable: !state
                                                            .emailIsEditable,
                                                      ),
                                                    );
                                              },
                                              child: Icon(
                                                Icons.edit,
                                                color: state.emailIsEditable
                                                    ? marronmedio
                                                    : enabledColor,
                                                size: 20,
                                              ),
                                            ),
                                          ),
                                        ),
                                      )
                                    ],
                                  )
                                : Container(),
                            if (!state.emailIsEditable || !state.nameIsEditable)
                              Container(
                                width: 150.pw,
                                height: 30.ph,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(15),
                                ),
                                child: ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                    backgroundColor: marronclarito,
                                  ),
                                  onPressed: () {
                                    try {
                                      FirebaseAuth.instance.currentUser!
                                          .updateEmail(emailController.text);
                                      updateUsuario(
                                          campo: 'name',
                                          newValue: nameController.text);
                                      updateUsuario(
                                          campo: 'lastname',
                                          newValue: apellidoController.text);
                                      updateUsuario(
                                          campo: 'email',
                                          newValue: emailController.text);
                                      context.read<BlocProfile>().add(
                                            const BlocProfileGetUserDataEvent(
                                              apellido: '',
                                              nombre: '',
                                              email: '',
                                            ),
                                          );
                                    } catch (e) {
                                      showDialog(
                                        context: context,
                                        builder: (context) => ShowAlertCustom(
                                          texto: 'Error: $e',
                                          onTap: () =>
                                              Navigator.of(context).pop,
                                        ),
                                      );
                                    }
                                  },
                                  child: Text(
                                    'Guardar',
                                    style: TextStyle(
                                      fontWeight: FontWeight.w700,
                                      fontSize: 17.pf,
                                    ),
                                  ),
                                ),
                              ),
                            EspacioVertical(height: 60.ph),
                            Column(
                              children: [
                                Transform.rotate(
                                  angle: math.pi / 2,
                                  child: const Icon(
                                    Icons.key_outlined,
                                    color: marronclarito,
                                    size: 50,
                                  ),
                                ),
                                EspacioVertical(height: 10.ph),
                                Container(
                                  width: 200.pw,
                                  height: 30.ph,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(15),
                                  ),
                                  child: ElevatedButton(
                                    style: ElevatedButton.styleFrom(
                                      backgroundColor: marronclarito,
                                    ),
                                    onPressed: () {
                                      showDialog(
                                        context: context,
                                        builder: (context) => ShowAlertCustom(
                                          texto:
                                              'Al presionar confirmar se le enviara un correo en el que podra cambiar su contraseña ingresando al link',
                                          onTap: () async {
                                            String email = await getFieldValue(
                                                colection: 'usuarios',
                                                campo: 'email');
                                            FirebaseAuth.instance
                                                .sendPasswordResetEmail(
                                              email: email,
                                            );
                                          },
                                        ),
                                      );
                                    },
                                    child: Text(
                                      'Cambiar contraseña',
                                      style: TextStyle(
                                        fontWeight: FontWeight.w700,
                                        fontSize: 17.pf,
                                      ),
                                    ),
                                  ),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                );
              },
            ),
          );
        }
        if (state.status == BlocProfileStatus.error) {
          showDialog(
            context: context,
            builder: (context) => ShowAlertCustom(
              texto: 'Error al obtener los datos del usuario',
              onTap: () async {
                Navigator.of(context).pop();
              },
            ),
          );
        }
        return Container();
      },
    );
  }
}

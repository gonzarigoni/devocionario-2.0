import 'package:devocionario2/constantes.dart';
import 'package:devocionario2/pages/6-progreso/bloc/bloc_progreso.dart';
import 'package:devocionario2/pages/6-progreso/bloc/bloc_progreso_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:full_responsive/full_responsive.dart';

class LlevasXdias extends StatelessWidget {
  const LlevasXdias({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(15),
      child: Container(
        width: 215.pw,
        height: 78.ph,
        color: Colors.white,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'Llevas:',
              style: TextStyle(
                fontSize: 18.pf,
                color: marronmedio,
              ),
            ),
            BlocBuilder<BlocProgreso, BlocProgresoState>(
              builder: (context, state) {
                return Text(
                  '${state.diasCumplidos} ${state.diasCumplidos == 1 ? 'día' : 'días'}',
                  style: TextStyle(
                    fontWeight: FontWeight.w500,
                    fontSize: 20.pf,
                    color: marronmedio,
                  ),
                );
              },
            )
          ],
        ),
      ),
    );
  }
}

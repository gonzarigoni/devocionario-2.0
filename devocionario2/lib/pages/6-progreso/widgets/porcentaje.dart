import 'package:devocionario2/constantes.dart';
import 'package:devocionario2/pages/6-progreso/bloc/bloc_progreso.dart';
import 'package:devocionario2/pages/6-progreso/bloc/bloc_progreso_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class Porcentaje extends StatefulWidget {
  const Porcentaje({
    super.key,
  });

  @override
  State<Porcentaje> createState() => _PorcentajeState();
}

class _PorcentajeState extends State<Porcentaje> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<BlocProgreso, BlocProgresoState>(
      builder: (context, state) {
        return Padding(
          padding: const EdgeInsets.all(28),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Stack(
                children: [
                  const ClipRRect(
                    child: Align(
                      alignment: Alignment.bottomCenter,
                      heightFactor: 1,
                      child: FaIcon(
                        FontAwesomeIcons.bookBible,
                        size: 100,
                        color: Color(0xffdfdfdf),
                      ),
                    ),
                  ),
                  Column(
                    children: [
                      EspacioVertical(
                        height: (100 -
                            ((state.diasCumplidos * 100) /
                                (state.aniosDevocion * 365))),
                      ),
                      ClipRRect(
                        child: Align(
                          alignment: Alignment.bottomCenter,
                          heightFactor: (((state.diasCumplidos * 100) /
                                  (state.aniosDevocion * 365)) /
                              100),
                          child: const FaIcon(
                            FontAwesomeIcons.bookBible,
                            size: 100,
                            color: marronclarito,
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              ),
              const EspacioHorizontal(
                width: 20,
              ),
              //Utilizamos el operador ~/ (división entera) para obtener la parte entera del resultado
              //(state.diasCumplidos * 100) / (state.aniosDevocion * 365). Si el resultado es un número
              //entero (es decir, el residuo % 1 es igual a 0), mostramos la parte entera seguida de '%'.
              //Si el resultado tiene decimales, utilizamos toStringAsFixed(2)
              Text(
                ((state.diasCumplidos * 100) /
                            (state.aniosDevocion * 365) %
                            1 ==
                        0)
                    ? '${(state.diasCumplidos * 100) ~/ (state.aniosDevocion * 365)}%'
                    : '${((state.diasCumplidos * 100) / (state.aniosDevocion * 365)).toStringAsFixed(2)}%',
                style: const TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: 20,
                  color: marronoscuro,
                ),
              )
            ],
          ),
        );
      },
    );
  }
}

import 'package:devocionario2/constantes.dart';
import 'package:devocionario2/pages/2-seleccionador_devocion/page_seleccionador_devocion.dart';
import 'package:devocionario2/pages/4-oraciones/page_oraciones.dart';
import 'package:devocionario2/pages/5-finalizacion/page_finalizacion.dart';
import 'package:devocionario2/pages/6-progreso/bloc/bloc_progreso.dart';
import 'package:devocionario2/pages/6-progreso/bloc/bloc_progreso_state.dart';
import 'package:devocionario2/services/setters.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:full_responsive/full_responsive.dart';

class BotonesBottomProgreso extends StatelessWidget {
  const BotonesBottomProgreso({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<BlocProgreso, BlocProgresoState>(
      builder: (context, state) {
        return Container(
          color: blanquito,
          height: 60.ph,
          child: Center(
            child: ((state.aniosDevocion * 365) == state.diasCumplidos)
                ? SizedBox(
                    height: 30.ph,
                    width: 180.pw,
                    child: OutlinedButton(
                      style: OutlinedButton.styleFrom(
                        side: const BorderSide(
                          width: 2,
                          color: marronclarito,
                        ),
                      ),
                      onPressed: () {
                        Navigator.of(context).pushReplacement(
                            MaterialPageRoute<PageSeleccionadorDevocion>(
                          builder: (BuildContext context) {
                            return const PageSeleccionadorDevocion();
                          },
                        ));
                      },
                      child: Text(
                        'Nueva devoción',
                        style: TextStyle(
                          color: marronclarito,
                          fontSize: 17.pf,
                        ),
                      ),
                    ),
                  )
                : Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      SizedBox(
                        height: 30.ph,
                        width: 120.pw,
                        child: OutlinedButton(
                          style: OutlinedButton.styleFrom(
                            side: const BorderSide(
                              width: 2,
                              color: marronclarito,
                            ),
                          ),
                          onPressed: () {
                            Navigator.of(context).pushReplacement(
                                MaterialPageRoute<PageFinalizacion>(
                              builder: (BuildContext context) {
                                return const PageFinalizacion();
                              },
                            ));
                          },
                          child: Text(
                            'Atrás',
                            style: TextStyle(
                              color: marronclarito,
                              fontSize: 17.pf,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 30.ph,
                        width: 120.pw,
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            backgroundColor: enabledColor,
                          ),
                          onPressed: () {
                            updateOracionActual(newValue: 0);
                            Navigator.of(context).pushReplacement(
                                MaterialPageRoute<PageOraciones>(
                              builder: (BuildContext context) {
                                return const PageOraciones();
                              },
                            ));
                          },
                          child: FittedBox(
                            child: Text(
                              'Volver a rezar',
                              style: TextStyle(
                                fontSize: 17.pf,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
          ),
        );
      },
    );
  }
}

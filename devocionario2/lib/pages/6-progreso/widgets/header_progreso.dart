import 'package:devocionario2/constantes.dart';
import 'package:devocionario2/pages/6-progreso/bloc/bloc_progreso.dart';
import 'package:devocionario2/pages/6-progreso/bloc/bloc_progreso_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:full_responsive/full_responsive.dart';

class HeaderProgreso extends StatelessWidget {
  const HeaderProgreso({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        BlocBuilder<BlocProgreso, BlocProgresoState>(
          builder: (context, state) {
            return Text(
              'Sta. Brígida de ${state.aniosDevocion} ${state.aniosDevocion == 1 ? 'año' : 'años'}',
              style: TextStyle(
                color: marronoscuro,
                fontSize: 23.pf,
                fontWeight: FontWeight.w700,
              ),
            );
          },
        ),
        EspacioVertical(height: 15.ph),
        BlocBuilder<BlocProgreso, BlocProgresoState>(builder: (context, state) {
          return Text(
            'Felicitaciones',
            style: TextStyle(
                color: marronmedio,
                fontWeight: FontWeight.w700,
                fontSize: 40.pf),
          );
        }),
        BlocBuilder<BlocProgreso, BlocProgresoState>(builder: (context, state) {
          return Text(
            ((state.aniosDevocion * 365) == state.diasCumplidos)
                ? 'Terminaste la devoción'
                : '',
            style: TextStyle(
                color: marronmedio,
                fontWeight: FontWeight.w700,
                fontSize: 25.pf),
          );
        }),
      ],
    );
  }
}

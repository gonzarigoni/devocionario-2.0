import 'package:devocionario2/constantes.dart';
import 'package:devocionario2/pages/6-progreso/bloc/bloc_progreso.dart';
import 'package:devocionario2/pages/6-progreso/bloc/bloc_progreso_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class FaltanXdias extends StatelessWidget {
  const FaltanXdias({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(15),
      child: Container(
        width: 215,
        height: 78,
        color: Colors.white,
        child: BlocBuilder<BlocProgreso, BlocProgresoState>(
          builder: (context, state) {
            return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  (state.aniosDevocion * 365) - state.diasCumplidos == 1
                      ? 'Falta:'
                      : 'Faltan:',
                  style: const TextStyle(
                    fontSize: 20,
                    color: marronmedio,
                  ),
                ),
                Text(
                  '${(state.aniosDevocion * 365) - state.diasCumplidos}  ${(state.aniosDevocion * 365) - state.diasCumplidos == 1 ? 'día' : 'días'}',
                  style: const TextStyle(
                    fontWeight: FontWeight.w500,
                    fontSize: 20,
                    color: marronmedio,
                  ),
                )
              ],
            );
          },
        ),
      ),
    );
  }
}

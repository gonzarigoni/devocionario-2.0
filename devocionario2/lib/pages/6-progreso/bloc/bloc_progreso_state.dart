// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:equatable/equatable.dart';

enum BlocProgresoStatus {
  /// estado inicial sin nada cargado
  initial,

  /// estado cargado
  success,

  /// estado de error
  error,

  /// estado cargando...
  loading;

  ///devuleve true si el estado es Success
  bool get isSuccess => this == BlocProgresoStatus.success;

  ///devuelve true si el estado es Error
  bool get isError => this == BlocProgresoStatus.error;

  ///devuleve true si el estado es Loading
  bool get isLoading => this == BlocProgresoStatus.loading;

  ///devuelve un estado de true al iniciar
  bool get isInitial => this == BlocProgresoStatus.initial;
}

class BlocProgresoState extends Equatable {
  const BlocProgresoState({
    required this.diasCumplidos,
    required this.aniosDevocion,
    required this.status,
    required this.ultimoDiaCumplido,
    required this.estaHabilitado,
  });

  final int diasCumplidos;
  final int aniosDevocion;
  final BlocProgresoStatus status;
  final String ultimoDiaCumplido;
  final bool estaHabilitado;

  @override
  List<Object> get props => [
        aniosDevocion,
        diasCumplidos,
        status,
        ultimoDiaCumplido,
        estaHabilitado,
      ];

  BlocProgresoState copyWith({
    final int? diasCumplidos,
    final int? aniosDevocion,
    final BlocProgresoStatus? status,
    final String? ultimoDiaCumplido,
    final bool? estaHabilitado,
  }) {
    return BlocProgresoState(
      aniosDevocion: aniosDevocion ?? this.aniosDevocion,
      diasCumplidos: diasCumplidos ?? this.diasCumplidos,
      status: status ?? this.status,
      ultimoDiaCumplido: ultimoDiaCumplido ?? this.ultimoDiaCumplido,
      estaHabilitado: estaHabilitado ?? this.estaHabilitado,
    );
  }
}

class ProgresoInitial extends BlocProgresoState {
  const ProgresoInitial({
    required int diasCumplidos,
    required int aniosDevocion,
    required BlocProgresoStatus status,
    required String ultimoDiaCumplido,
    required bool estaHabilitado,
  }) : super(
          diasCumplidos: diasCumplidos,
          aniosDevocion: aniosDevocion,
          status: BlocProgresoStatus.initial,
          ultimoDiaCumplido: ultimoDiaCumplido,
          estaHabilitado: estaHabilitado,
        );
}

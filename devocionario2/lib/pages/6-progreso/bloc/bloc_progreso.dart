import 'dart:async';
import 'package:devocionario2/pages/6-progreso/bloc/bloc_progreso_event.dart';
import 'package:devocionario2/pages/6-progreso/bloc/bloc_progreso_state.dart';
import 'package:devocionario2/services/getters.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:intl/intl.dart';

class BlocProgreso extends Bloc<BlocProgresoEvent, BlocProgresoState> {
  BlocProgreso()
      : super(const ProgresoInitial(
          diasCumplidos: 1,
          aniosDevocion: 1,
          ultimoDiaCumplido: '',
          status: BlocProgresoStatus.initial,
          estaHabilitado: false,
        )) {
    on<BlocProgresoGetUserDataEvent>(_getUserData);
  }

  FutureOr<void> _getUserData(BlocProgresoGetUserDataEvent event,
      Emitter<BlocProgresoState> emit) async {
    emit(state.copyWith(status: BlocProgresoStatus.loading));
    try {
      Map<String, dynamic> response = await getUserDataProgreso();
      final int? aniosDevocion = response['aniosDevocion'];
      final int? diasCumplidos = response['diasCumplidos'];
      final String? ultimoDiaCumplido = response['ultimoDiaCumplido'];

      bool? estaHabilitado;

      DateTime hoy = DateTime.now();
      String horaDeHoy = DateFormat('HH').format(hoy);
      String fechaAComparar;

      if (int.parse(horaDeHoy) < 5) {
        fechaAComparar = DateFormat('yyyyMMdd')
            .format(hoy.subtract(const Duration(days: 1)));
      } else {
        fechaAComparar = DateFormat('yyyyMMdd').format(hoy);
      }
      if (fechaAComparar != ultimoDiaCumplido) {
        estaHabilitado = true;
      } else {
        estaHabilitado = false;
      }
      //  estaHabilitado = fechaAComparar != ultimoDiaCumplido;

      emit(
        state.copyWith(
          aniosDevocion: aniosDevocion,
          diasCumplidos: diasCumplidos,
          ultimoDiaCumplido: ultimoDiaCumplido,
          status: BlocProgresoStatus.success,
          estaHabilitado: estaHabilitado,
        ),
      );
    } catch (e) {
      emit(state.copyWith(
        status: BlocProgresoStatus.error,
      ));
    }
  }
}

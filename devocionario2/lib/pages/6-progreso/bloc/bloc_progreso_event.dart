// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:equatable/equatable.dart';

abstract class BlocProgresoEvent extends Equatable {
  const BlocProgresoEvent();

  @override
  List<Object> get props => [];
}

class BlocProgresoGetUserDataEvent extends BlocProgresoEvent {
  const BlocProgresoGetUserDataEvent({
    required this.diasCumplidos,
    required this.aniosDevocion,
    this.ultimoDiaCumplido,
    this.estaHabilitado,
  });
  final int? diasCumplidos;
  final int? aniosDevocion;
  final String? ultimoDiaCumplido;
  final bool? estaHabilitado;
}

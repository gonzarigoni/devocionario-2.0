import 'package:auto_route/auto_route.dart';
import 'package:devocionario2/constantes.dart';
import 'package:devocionario2/pages/6-progreso/bloc/bloc_progreso.dart';
import 'package:devocionario2/pages/6-progreso/bloc/bloc_progreso_event.dart';
import 'package:devocionario2/pages/6-progreso/bloc/bloc_progreso_state.dart';
import 'package:devocionario2/pages/6-progreso/widgets/botones_bottom_progreso.dart';
import 'package:devocionario2/pages/6-progreso/widgets/faltan_x_dias.dart';
import 'package:devocionario2/pages/6-progreso/widgets/header_progreso.dart';
import 'package:devocionario2/pages/6-progreso/widgets/llevas_x_dias.dart';
import 'package:devocionario2/pages/6-progreso/widgets/porcentaje.dart';
import 'package:devocionario2/services/setters.dart';
import 'package:devocionario2/widgets_global/appbar_custom.dart';
import 'package:devocionario2/widgets_global/dialog_custom.dart';
import 'package:devocionario2/widgets_global/drawer_custom.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:full_responsive/full_responsive.dart';

@RoutePage()
class PageProgreso extends StatefulWidget {
  const PageProgreso({super.key});

  @override
  State<PageProgreso> createState() => _PageProgresoState();
}

class _PageProgresoState extends State<PageProgreso> {
  @override
  void initState() {
    updatePaginaActual(newValue: 6);
    context.read<BlocProgreso>().add(
        const BlocProgresoGetUserDataEvent(aniosDevocion: 0, diasCumplidos: 0));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<BlocProgreso, BlocProgresoState>(
      builder: (context, state) {
        if (state.status == BlocProgresoStatus.loading) {
          return const Scaffold(
            body: Center(
                child: CircularProgressIndicator(
              color: marronmedio,
              backgroundColor: Color(0xffdddddd),
            )),
          );
        }
        if (state.status == BlocProgresoStatus.success) {
          return Scaffold(
            appBar: const AppBarCustom(),
            drawer: const DrawerCustom(),
            body: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const HeaderProgreso(),
                const LlevasXdias(),
                const Porcentaje(),
                const FaltanXdias(),
                EspacioVertical(height: 60.ph)
              ],
            ),
            bottomSheet: const BotonesBottomProgreso(),
          );
        }
        if (state.status == BlocProgresoStatus.error) {
          showDialog(
            context: context,
            builder: (context) => ShowAlertCustom(
              texto: 'Error al obtener los datos del usuario',
              onTap: () async {
                Navigator.of(context).pop();
              },
            ),
          );
        }
        return Container();
      },
    );
  }
}

import 'package:devocionario2/constantes.dart';
import 'package:devocionario2/pages/7-promesas/bloc/bloc_promesas.dart';
import 'package:devocionario2/pages/7-promesas/bloc/bloc_promesas_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:full_responsive/full_responsive.dart';

class HeaderPromesas extends StatelessWidget {
  const HeaderPromesas({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        BlocBuilder<BlocPromesas, BlocPromesasState>(
          builder: (context, state) {
            return Text(
              'Sta. Brígida de ${state.aniosDevocion} ${state.aniosDevocion == 1 ? 'año' : 'años'}',
              style: TextStyle(
                color: marronoscuro,
                fontSize: 23.pf,
                fontWeight: FontWeight.w700,
              ),
            );
          },
        ),
        EspacioVertical(height: 5.ph),
        Text(
          'Fecha de comienzo',
          style: TextStyle(
            color: marronoscuro,
            fontSize: 14.pf,
            fontWeight: FontWeight.w600,
          ),
        ),
      ],
    );
  }
}

import 'package:devocionario2/constantes.dart';
import 'package:devocionario2/pages/7-promesas/bloc/bloc_promesas.dart';
import 'package:devocionario2/pages/7-promesas/bloc/bloc_promesas_event.dart';
import 'package:devocionario2/pages/7-promesas/bloc/bloc_promesas_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:full_responsive/full_responsive.dart';

class PersonasElegidaPromesasTextFields extends StatelessWidget {
  const PersonasElegidaPromesasTextFields({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<BlocPromesas, BlocPromesasState>(
      builder: (context, state) {
        return Padding(
          padding: EdgeInsets.only(top: 10.ph),
          child: ConstrainedBox(
            constraints: BoxConstraints(maxWidth: 400.pw),
            child: Column(
              children: [
                SizedBox(
                  height: 40.ph,
                  child: TextFormField(
                    initialValue: state.personaElegida1,
                    maxLength: 50,
                    style: const TextStyle(color: disabledColor),
                    obscureText: state.isObscure0,
                    readOnly: true,
                    decoration: InputDecoration(
                      counterText: '',
                      enabledBorder: const OutlineInputBorder(
                        borderSide: BorderSide(
                          color: disabledColor,
                        ),
                      ),
                      focusedBorder: const OutlineInputBorder(
                        borderSide: BorderSide(
                          color: disabledColor,
                        ),
                      ),
                      hintStyle: TextStyle(
                        fontSize: 15.pf,
                        color: disabledColor,
                      ),
                      contentPadding: EdgeInsets.symmetric(
                          vertical: 5.ph, horizontal: 10.pw),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5),
                      ),
                      filled: true,
                      fillColor: Colors.white,
                      suffixIcon: IconButton(
                        icon: Icon(
                          state.isObscure0
                              ? Icons.visibility_off_outlined
                              : Icons.visibility_outlined,
                          color: marronclarito,
                        ),
                        onPressed: () {
                          context.read<BlocPromesas>().add(
                                BlocPromesasObscureEvent(
                                  isObscure0: !state.isObscure0,
                                  isObscure1: state.isObscure1,
                                  isObscure2: state.isObscure2,
                                ),
                              );
                        },
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 8.ph),
                  child: SizedBox(
                    height: 40.ph,
                    child: TextFormField(
                      initialValue: state.personaElegida2,
                      maxLength: 40,
                      style: const TextStyle(color: disabledColor),
                      obscureText: state.isObscure1,
                      readOnly: true,
                      decoration: InputDecoration(
                        counterText: '',
                        enabledBorder: const OutlineInputBorder(
                          borderSide: BorderSide(
                            color: disabledColor,
                          ),
                        ),
                        focusedBorder: const OutlineInputBorder(
                          borderSide: BorderSide(
                            color: disabledColor,
                          ),
                        ),
                        hintStyle: TextStyle(
                          fontSize: 15.pf,
                          color: disabledColor,
                        ),
                        contentPadding: EdgeInsets.symmetric(
                            vertical: 5.ph, horizontal: 10.pw),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                        ),
                        filled: true,
                        fillColor: Colors.white,
                        suffixIcon: IconButton(
                          icon: Icon(
                            state.isObscure1
                                ? Icons.visibility_off_outlined
                                : Icons.visibility_outlined,
                            color: marronclarito,
                          ),
                          onPressed: () {
                            context
                                .read<BlocPromesas>()
                                .add(BlocPromesasObscureEvent(
                                  isObscure0: state.isObscure0,
                                  isObscure1: !state.isObscure1,
                                  isObscure2: state.isObscure2,
                                ));
                          },
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 40.ph,
                  child: TextFormField(
                    initialValue: state.personaElegida3,
                    maxLength: 40,
                    style: const TextStyle(color: disabledColor),
                    obscureText: state.isObscure2,
                    readOnly: true,
                    decoration: InputDecoration(
                      counterText: '',
                      enabledBorder: const OutlineInputBorder(
                        borderSide: BorderSide(
                          color: disabledColor,
                        ),
                      ),
                      focusedBorder: const OutlineInputBorder(
                        borderSide: BorderSide(
                          color: disabledColor,
                        ),
                      ),
                      hintStyle: TextStyle(
                        fontSize: 15.pf,
                        color: disabledColor,
                      ),
                      contentPadding: EdgeInsets.symmetric(
                          vertical: 5.ph, horizontal: 10.pw),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5),
                      ),
                      filled: true,
                      fillColor: Colors.white,
                      suffixIcon: IconButton(
                        icon: Icon(
                          state.isObscure2
                              ? Icons.visibility_off_outlined
                              : Icons.visibility_outlined,
                          color: marronclarito,
                        ),
                        onPressed: () {
                          context.read<BlocPromesas>().add(
                                BlocPromesasObscureEvent(
                                  isObscure0: state.isObscure0,
                                  isObscure1: state.isObscure1,
                                  isObscure2: !state.isObscure2,
                                ),
                              );
                        },
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}

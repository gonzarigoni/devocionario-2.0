import 'package:devocionario2/constantes.dart';
import 'package:devocionario2/pages/4-oraciones/page_oraciones.dart';
import 'package:devocionario2/pages/5-finalizacion/page_finalizacion.dart';
import 'package:devocionario2/pages/6-progreso/page_progreso.dart';
import 'package:devocionario2/pages/8-profile/page_profile.dart';
import 'package:devocionario2/pages/7-promesas/bloc/bloc_promesas.dart';
import 'package:devocionario2/pages/7-promesas/bloc/bloc_promesas_state.dart';
import 'package:devocionario2/pages/9-configuracion/page_configuracion.dart';
import 'package:devocionario2/services/getters.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:full_responsive/full_responsive.dart';

class BotonBottom extends StatelessWidget {
  const BotonBottom({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<BlocPromesas, BlocPromesasState>(
      builder: (context, state) {
        return Container(
          color: blanquito,
          height: 60.ph,
          child: Center(
            child: SizedBox(
              height: 30.ph,
              width: 120.pw,
              child: OutlinedButton(
                style: OutlinedButton.styleFrom(
                  side: BorderSide(
                    width: 2.pw,
                    color: marronclarito,
                  ),
                ),
                onPressed: () async {
                  var widget = await irPaginaAnterior();
                  Navigator.of(context).pushReplacement(
                      MaterialPageRoute(builder: (BuildContext context) {
                    return widget;
                  }));
                },
                child: Text(
                  'Atrás',
                  style: TextStyle(
                    color: marronclarito,
                    fontSize: 17.pf,
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}

Future<Widget> irPaginaAnterior() async {
  var asd =
      await getFieldValue(colection: 'devociones', campo: 'paginaAnterior');
  switch (asd) {
    case 4:
      return const PageOraciones();
    case 5:
      return const PageFinalizacion();
    case 6:
      return const PageProgreso();
    case 8:
      return const PageProfile();
    case 9:
      return const PageConfiguracion();
    default:
      return const PageOraciones();
  }
}

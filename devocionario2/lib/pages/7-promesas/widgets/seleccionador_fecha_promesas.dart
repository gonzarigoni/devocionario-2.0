import 'package:devocionario2/constantes.dart';
import 'package:devocionario2/pages/7-promesas/bloc/bloc_promesas.dart';
import 'package:devocionario2/pages/7-promesas/bloc/bloc_promesas_event.dart';
import 'package:devocionario2/pages/7-promesas/bloc/bloc_promesas_state.dart';
import 'package:devocionario2/services/setters.dart';
import 'package:devocionario2/widgets_global/dialog_custom.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:full_responsive/full_responsive.dart';
import 'package:intl/intl.dart';

class SeleccionadorFechaPromesas extends StatelessWidget {
  const SeleccionadorFechaPromesas({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<BlocPromesas, BlocPromesasState>(
      builder: (context, state) {
        return GestureDetector(
          onTap: () async {
            DateTime fechaTemporal = state.fechaComienzo;
            DateTime? fechaSeleccionada = await showDatePicker(
              context: context,
              initialDate: state.fechaComienzo,
              firstDate: DateTime(2010),
              lastDate: DateTime.now(),
            );

            if (fechaSeleccionada != null &&
                formatDate(fechaTemporal) != formatDate(fechaSeleccionada)) {
              // ignore: use_build_context_synchronously
              showDialog(
                context: context,
                builder: (context) => ShowAlertCustom(
                  texto:
                      'Está a punto de continuar,\n¿Esta seguro de cambiar la fecha de inicio?',
                  onTap: () async {
                    context.read<BlocPromesas>().add(
                        BlocPromesasCambiarFechaEvent(
                            fecha: fechaSeleccionada));
                    try {
                      updateFechaSeleccionada(newValue: fechaSeleccionada);
                    } catch (e) {
                      showDialog(
                        context: context,
                        builder: (context) => ShowAlertCustom(
                          texto: 'No se pudo actualizar la fecha de inicio $e',
                          onTap: () => Navigator.of(context).pop,
                        ),
                      );
                    }

                    Navigator.of(context).pop();
                  },
                ),
              );
            }
          },
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
              color: blanquito,
            ),
            width: 220.pw,
            height: 35.ph,
            child: Center(
              child: Text(
                '${state.fechaComienzo.day}/${state.fechaComienzo.month}/${state.fechaComienzo.year}',
                style: TextStyle(
                  color: fontColorDisabled,
                  fontSize: 16.pf,
                  fontWeight: FontWeight.w400,
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}

String formatDate(DateTime dateTime) {
  DateFormat dateFormat = DateFormat('dd/MM/yyyy');
  return dateFormat.format(dateTime);
}

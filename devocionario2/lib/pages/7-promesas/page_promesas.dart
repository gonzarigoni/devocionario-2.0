import 'package:auto_route/auto_route.dart';
import 'package:devocionario2/constantes.dart';
import 'package:devocionario2/pages/7-promesas/bloc/bloc_promesas.dart';
import 'package:devocionario2/pages/7-promesas/bloc/bloc_promesas_event.dart';
import 'package:devocionario2/pages/7-promesas/bloc/bloc_promesas_state.dart';
import 'package:devocionario2/pages/7-promesas/widgets/boton_bottom.dart';
import 'package:devocionario2/pages/7-promesas/widgets/container_promesas.dart';
import 'package:devocionario2/pages/7-promesas/widgets/header_promesas.dart';
import 'package:devocionario2/pages/7-promesas/widgets/seleccionador_fecha_promesas.dart';
import 'package:devocionario2/services/setters.dart';
import 'package:devocionario2/widgets_global/dialog_custom.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:full_responsive/full_responsive.dart';

@RoutePage()
class PagePromesas extends StatefulWidget {
  const PagePromesas({
    Key? key,
  }) : super(key: key);

  @override
  State<PagePromesas> createState() => _PagePromesasState();
}

class _PagePromesasState extends State<PagePromesas> {
  @override
  void initState() {
    updatePaginaActual(newValue: 7);
    context.read<BlocPromesas>().add(BlocPromesasGetUserDataEvent(
        aniosDevocion: 0, fechaComienzo: DateTime.now()));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<BlocPromesas, BlocPromesasState>(
      builder: (context, state) {
        if (state.status == BlocPromesasStatus.loading) {
          return const Scaffold(
            body: Center(
                child: CircularProgressIndicator(
              color: marronmedio,
              backgroundColor: Color(0xffdddddd),
            )),
          );
        }
        if (state.status == BlocPromesasStatus.success) {
          return Scaffold(
            backgroundColor: marronclarito,
            appBar: AppBar(
              title: Text(
                'Oración',
                style: TextStyle(
                  color: marronoscuro,
                  fontSize: 23.pf,
                  fontWeight: FontWeight.w400,
                ),
              ),
              elevation: 0,
              centerTitle: true,
              toolbarHeight: 40.ph,
              backgroundColor: Colors.transparent,
              leading: GestureDetector(
                onTap: () async {
                  var widget = await irPaginaAnterior();
                  Navigator.of(context).pushReplacement(
                      MaterialPageRoute(builder: (BuildContext context) {
                    return widget;
                  }));
                },
                child: const Icon(Icons.arrow_back),
              ),
            ),
            body: Container(
              decoration: const BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [
                    marronclarito,
                    marronclarito,
                    marronclarito,
                    blanquito,
                    blanquito,
                    blanquito
                  ],
                ),
              ),
              width: MediaQuery.of(context).size.width,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  const HeaderPromesas(),
                  EspacioVertical(height: 5.ph),
                  const SeleccionadorFechaPromesas(),
                  EspacioVertical(height: 10.ph),
                  const ContainerPromesas(),
                  EspacioVertical(height: 60.ph),
                ],
              ),
            ),
            bottomSheet: const BotonBottom(),
          );
        }
        if (state.status == BlocPromesasStatus.error) {
          showDialog(
            context: context,
            builder: (context) => ShowAlertCustom(
              texto: 'Error al obtener los datos del usuario',
              onTap: () async {
                Navigator.of(context).pop();
              },
            ),
          );
        }
        return Container();
      },
    );
  }
}

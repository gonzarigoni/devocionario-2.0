import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:devocionario2/pages/7-promesas/bloc/bloc_promesas_event.dart';
import 'package:devocionario2/pages/7-promesas/bloc/bloc_promesas_state.dart';
import 'package:devocionario2/services/getters.dart';
import 'package:devocionario2/services/setters.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class BlocPromesas extends Bloc<BlocPromesasEvent, BlocPromesasState> {
  BlocPromesas()
      : super(PromesasInitial(
          fechaComienzo: DateTime.now(),
          isObscure0: false,
          isObscure1: false,
          isObscure2: false,
          aniosDevocion: 0,
          personaElegida1: '',
          personaElegida2: '',
          personaElegida3: '',
          status: BlocPromesasStatus.initial,
        )) {
    on<BlocPromesasCambiarFechaEvent>(_cambiarFecha);
    on<BlocPromesasObscureEvent>(_cambiarObscures);
    on<BlocPromesasGetUserDataEvent>(_getUserData);
  }

  Future<void> _cambiarFecha(
    BlocPromesasCambiarFechaEvent event,
    Emitter<BlocPromesasState> emit,
  ) async {
    try {
      final DateTime? fecha = event.fecha;
      emit(state.copyWith(fechaComienzo: fecha));
    } catch (e) {
      print(e);
    }
  }

  FutureOr<void> _cambiarObscures(
      BlocPromesasObscureEvent event, Emitter<BlocPromesasState> emit) {
    try {
      final bool isObscure0 = event.isObscure0;
      final bool isObscure1 = event.isObscure1;
      final bool isObscure2 = event.isObscure2;
      updateObscures(
          personaElegidaObscure1: isObscure0,
          personaElegidaObscure2: isObscure1,
          personaElegidaObscure3: isObscure2);
      emit(
        state.copyWith(
          isObscure0: isObscure0,
          isObscure1: isObscure1,
          isObscure2: isObscure2,
        ),
      );
    } catch (e) {
      print(e);
    }
  }

  FutureOr<void> _getUserData(BlocPromesasGetUserDataEvent event,
      Emitter<BlocPromesasState> emit) async {
    emit(state.copyWith(status: BlocPromesasStatus.loading));
    try {
      Map<String, dynamic> response = await getUserDataPromesas();
      final int? aniosDevocion = response['aniosDevocion'];
      final Timestamp? fechaComienzo = response['fechaComienzo'];
      final String? personaElegida1 = response['personaElegida1'];
      final String? personaElegida2 = response['personaElegida2'];
      final String? personaElegida3 = response['personaElegida3'];
      final bool personaElegidaObscure1 = response['personaElegidaObscure1'];
      final bool personaElegidaObscure2 = response['personaElegidaObscure2'];
      final bool personaElegidaObscure3 = response['personaElegidaObscure3'];

      DateTime dateTime = fechaComienzo!.toDate();
      emit(
        state.copyWith(
          aniosDevocion: aniosDevocion,
          fechaComienzo: dateTime,
          personaElegida1: personaElegida1,
          personaElegida2: personaElegida2,
          personaElegida3: personaElegida3,
          isObscure0: personaElegidaObscure1,
          isObscure1: personaElegidaObscure2,
          isObscure2: personaElegidaObscure3,
          status: BlocPromesasStatus.success,
        ),
      );
    } catch (e) {
      emit(state.copyWith(status: BlocPromesasStatus.error));
    }
  }
}

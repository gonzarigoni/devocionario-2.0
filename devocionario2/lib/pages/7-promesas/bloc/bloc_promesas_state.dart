// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:equatable/equatable.dart';

enum BlocPromesasStatus {
  /// estado inicial sin nada cargado
  initial,

  /// estado cargado
  success,

  /// estado de error
  error,

  /// estado cargando...
  loading;

  ///devuleve true si el estado es Success
  bool get isSuccess => this == BlocPromesasStatus.success;

  ///devuelve true si el estado es Error
  bool get isError => this == BlocPromesasStatus.error;

  ///devuleve true si el estado es Loading
  bool get isLoading => this == BlocPromesasStatus.loading;

  ///devuelve un estado de true al iniciar
  bool get isInitial => this == BlocPromesasStatus.initial;
}

class BlocPromesasState extends Equatable {
  const BlocPromesasState({
    required this.fechaComienzo,
    required this.isObscure0,
    required this.isObscure1,
    required this.isObscure2,
    required this.aniosDevocion,
    required this.personaElegida1,
    required this.personaElegida2,
    required this.personaElegida3,
    required this.status,
  });

  final DateTime fechaComienzo;
  final bool isObscure0;
  final bool isObscure1;
  final bool isObscure2;
  final int aniosDevocion;
  final String personaElegida1;
  final String personaElegida2;
  final String personaElegida3;
  final BlocPromesasStatus status;
  @override
  List<Object> get props => [
        fechaComienzo,
        isObscure0,
        isObscure1,
        isObscure2,
        aniosDevocion,
        personaElegida1,
        personaElegida2,
        personaElegida3,
        status,
      ];

  BlocPromesasState copyWith({
    DateTime? fechaComienzo,
    bool? isObscure0,
    bool? isObscure1,
    bool? isObscure2,
    int? aniosDevocion,
    String? personaElegida1,
    String? personaElegida2,
    String? personaElegida3,
    BlocPromesasStatus? status,
  }) {
    return BlocPromesasState(
      fechaComienzo: fechaComienzo ?? this.fechaComienzo,
      isObscure0: isObscure0 ?? this.isObscure0,
      isObscure1: isObscure1 ?? this.isObscure1,
      isObscure2: isObscure2 ?? this.isObscure2,
      aniosDevocion: aniosDevocion ?? this.aniosDevocion,
      personaElegida1: personaElegida1 ?? this.personaElegida1,
      personaElegida2: personaElegida2 ?? this.personaElegida2,
      personaElegida3: personaElegida3 ?? this.personaElegida3,
      status: status ?? this.status,
    );
  }
}

class PromesasInitial extends BlocPromesasState {
  const PromesasInitial({
    required DateTime fechaComienzo,
    required bool isObscure0,
    required bool isObscure1,
    required bool isObscure2,
    required int aniosDevocion,
    required String personaElegida1,
    required String personaElegida2,
    required String personaElegida3,
    required BlocPromesasStatus status,
  }) : super(
          fechaComienzo: fechaComienzo,
          isObscure0: isObscure0,
          isObscure1: isObscure1,
          isObscure2: isObscure2,
          aniosDevocion: aniosDevocion,
          personaElegida1: personaElegida1,
          personaElegida2: personaElegida2,
          personaElegida3: personaElegida3,
          status: BlocPromesasStatus.initial,
        );
}

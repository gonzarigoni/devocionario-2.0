// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:equatable/equatable.dart';

abstract class BlocPromesasEvent extends Equatable {
  const BlocPromesasEvent();

  @override
  List<Object> get props => [];
}

class BlocPromesasCambiarFechaEvent extends BlocPromesasEvent {
  const BlocPromesasCambiarFechaEvent({
    required this.fecha,
  });
  final DateTime? fecha;
}

class BlocPromesasObscureEvent extends BlocPromesasEvent {
  const BlocPromesasObscureEvent({
    required this.isObscure0,
    required this.isObscure1,
    required this.isObscure2,
  });
  final bool isObscure0;
  final bool isObscure1;
  final bool isObscure2;
}

class BlocPromesasGetUserDataEvent extends BlocPromesasEvent {
  const BlocPromesasGetUserDataEvent({
    required this.aniosDevocion,
    required this.fechaComienzo,
    this.personaElegida1,
    this.personaElegida2,
    this.personaElegida3,
    this.personaElegidaObscure1 = '',
    this.personaElegidaObscure2 = '',
    this.personaElegidaObscure3 = '',
  });
  final int? aniosDevocion;
  final DateTime? fechaComienzo;
  final String? personaElegida1;
  final String? personaElegida2;
  final String? personaElegida3;
  final String personaElegidaObscure1;
  final String personaElegidaObscure2;
  final String personaElegidaObscure3;
}

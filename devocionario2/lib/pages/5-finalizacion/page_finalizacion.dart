import 'package:auto_route/auto_route.dart';
import 'package:devocionario2/constantes.dart';
import 'package:devocionario2/pages/5-finalizacion/bloc/bloc_finalizacion.dart';
import 'package:devocionario2/pages/5-finalizacion/bloc/bloc_finalizacion_event.dart';
import 'package:devocionario2/pages/5-finalizacion/bloc/bloc_finalizacion_state.dart';
import 'package:devocionario2/pages/5-finalizacion/widgets/boton_bottom_finalizacion.dart';
import 'package:devocionario2/pages/5-finalizacion/widgets/header_finalizacion.dart';
import 'package:devocionario2/services/setters.dart';
import 'package:devocionario2/widgets_global/appbar_custom.dart';
import 'package:devocionario2/widgets_global/dialog_custom.dart';
import 'package:devocionario2/widgets_global/drawer_custom.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

@RoutePage()
class PageFinalizacion extends StatefulWidget {
  const PageFinalizacion({
    super.key,
  });

  @override
  State<PageFinalizacion> createState() => _PageFinalizacionState();
}

class _PageFinalizacionState extends State<PageFinalizacion> {
  @override
  void initState() {
    updatePaginaActual(newValue: 5);
    context.read<BlocFinalizacion>().add(BlocFinalizacionGetUserDataEvent(
          aniosDevocion: 0,
          fechaComienzo: DateTime.now(),
          ultimoDiaCumplido: '',
        ));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<BlocFinalizacion, BlocFinalizacionState>(
      builder: (context, state) {
        if (state.status == BlocFinalizacionStatus.loading) {
          return const Scaffold(
            body: Center(
                child: CircularProgressIndicator(
              color: marronmedio,
              backgroundColor: Color(0xffdddddd),
            )),
          );
        }
        if (state.status == BlocFinalizacionStatus.success) {
          return SafeArea(
            child: Scaffold(
              appBar: const AppBarCustom(),
              drawer: const DrawerCustom(),
              body: SizedBox(
                width: MediaQuery.of(context).size.width,
                child: const HeaderFinalizacion(),
              ),
              bottomSheet: const BotonBottomFinalizacion(),
            ),
          );
        }
        if (state.status == BlocFinalizacionStatus.error) {
          showDialog(
            context: context,
            builder: (context) => ShowAlertCustom(
              texto: 'Error al obtener los datos del usuario',
              onTap: () async {
                Navigator.of(context).pop();
              },
            ),
          );
        }
        return Container();
      },
    );
  }
}

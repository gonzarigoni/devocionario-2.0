import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:devocionario2/pages/5-finalizacion/bloc/bloc_finalizacion_event.dart';
import 'package:devocionario2/pages/5-finalizacion/bloc/bloc_finalizacion_state.dart';
import 'package:devocionario2/services/getters.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class BlocFinalizacion
    extends Bloc<BlocFinalizacionEvent, BlocFinalizacionState> {
  BlocFinalizacion()
      : super(FinalizacionInitial(
            fechaComienzo: DateTime.now(),
            aniosDevocion: 0,
            ultimoDiaCumplido: '',
            status: BlocFinalizacionStatus.initial)) {
    on<BlocFinalizacionGetUserDataEvent>(_getUserData);
  }

  FutureOr<void> _getUserData(BlocFinalizacionGetUserDataEvent event,
      Emitter<BlocFinalizacionState> emit) async {
    emit(state.copyWith(status: BlocFinalizacionStatus.loading));
    try {
      Map<String, dynamic> response = await getUserDataPromesas();
      final int? aniosDevocion = response['aniosDevocion'];
      final Timestamp? fechaComienzo = response['fechaComienzo'];
      final String? ultimoDiaCumplido = response['ultimoDiaCumplido'];

      DateTime dateTimeFechaComienzo = fechaComienzo!.toDate();

      emit(
        state.copyWith(
          aniosDevocion: aniosDevocion,
          fechaComienzo: dateTimeFechaComienzo,
          ultimoDiaCumplido: ultimoDiaCumplido,
          status: BlocFinalizacionStatus.success,
        ),
      );
    } catch (e) {
      emit(state.copyWith(
        status: BlocFinalizacionStatus.error,
      ));
    }
  }
}

// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:equatable/equatable.dart';

enum BlocFinalizacionStatus {
  /// estado inicial sin nada cargado
  initial,

  /// estado cargado
  success,

  /// estado de error
  error,

  /// estado cargando...
  loading;

  ///devuleve true si el estado es Success
  bool get isSuccess => this == BlocFinalizacionStatus.success;

  ///devuelve true si el estado es Error
  bool get isError => this == BlocFinalizacionStatus.error;

  ///devuleve true si el estado es Loading
  bool get isLoading => this == BlocFinalizacionStatus.loading;

  ///devuelve un estado de true al iniciar
  bool get isInitial => this == BlocFinalizacionStatus.initial;
}

class BlocFinalizacionState extends Equatable {
  const BlocFinalizacionState({
    required this.fechaComienzo,
    required this.ultimoDiaCumplido,
    required this.aniosDevocion,
    required this.status,
  });

  final DateTime fechaComienzo;
  final String ultimoDiaCumplido;
  final int aniosDevocion;
  final BlocFinalizacionStatus status;
  @override
  List<Object> get props => [
        fechaComienzo,
        aniosDevocion,
        ultimoDiaCumplido,
        status,
      ];

  BlocFinalizacionState copyWith({
    DateTime? fechaComienzo,
    String? ultimoDiaCumplido,
    int? aniosDevocion,
    BlocFinalizacionStatus? status,
  }) {
    return BlocFinalizacionState(
      fechaComienzo: fechaComienzo ?? this.fechaComienzo,
      aniosDevocion: aniosDevocion ?? this.aniosDevocion,
      ultimoDiaCumplido: ultimoDiaCumplido ?? this.ultimoDiaCumplido,
      status: status ?? this.status,
    );
  }
}

class FinalizacionInitial extends BlocFinalizacionState {
  const FinalizacionInitial({
    required DateTime fechaComienzo,
    required String ultimoDiaCumplido,
    required int aniosDevocion,
    required BlocFinalizacionStatus status,
  }) : super(
          fechaComienzo: fechaComienzo,
          aniosDevocion: aniosDevocion,
          ultimoDiaCumplido: ultimoDiaCumplido,
          status: BlocFinalizacionStatus.initial,
        );
}

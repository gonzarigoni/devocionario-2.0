// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:equatable/equatable.dart';

abstract class BlocFinalizacionEvent extends Equatable {
  const BlocFinalizacionEvent();

  @override
  List<Object> get props => [];
}

class BlocFinalizacionGetUserDataEvent extends BlocFinalizacionEvent {
  const BlocFinalizacionGetUserDataEvent({
    required this.aniosDevocion,
    required this.fechaComienzo,
    required this.ultimoDiaCumplido,
  });
  final int? aniosDevocion;
  final DateTime? fechaComienzo;
  final String? ultimoDiaCumplido;
}

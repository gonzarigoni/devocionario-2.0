import 'package:devocionario2/constantes.dart';
import 'package:devocionario2/pages/4-oraciones/page_oraciones.dart';
import 'package:devocionario2/pages/5-finalizacion/bloc/bloc_finalizacion.dart';
import 'package:devocionario2/pages/5-finalizacion/bloc/bloc_finalizacion_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:full_responsive/full_responsive.dart';

class HeaderFinalizacion extends StatelessWidget {
  const HeaderFinalizacion({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        BlocBuilder<BlocFinalizacion, BlocFinalizacionState>(
          builder: (context, state) {
            return Text(
              'Sta. Brígida de ${state.aniosDevocion} ${state.aniosDevocion == 1 ? 'año' : 'años'}',
              style: TextStyle(
                color: marronoscuro,
                fontSize: 23.pf,
                fontWeight: FontWeight.w700,
              ),
            );
          },
        ),
        EspacioVertical(height: 10.ph),
        BlocBuilder<BlocFinalizacion, BlocFinalizacionState>(
          builder: (context, state) {
            return Text(
              '${state.fechaComienzo.day}/${state.fechaComienzo.month}/${state.fechaComienzo.year}',
              style: TextStyle(
                color: marronclarito,
                fontSize: 20.pf,
                fontWeight: FontWeight.w700,
              ),
            );
          },
        ),
        SizedBox(
          width: 300.pw,
          height: 40.ph,
          child: const ProgressBarView(
            value: 1,
          ),
        ),
        EspacioVertical(height: 20.ph),
        Text(
          '¡Listo por hoy!',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontWeight: FontWeight.w700,
            fontSize: 40.pf,
            color: marronclarito,
          ),
        ),
        EspacioVertical(
          height: 20.ph,
        ),
        Text(
          'Terminaste tus oraciones del día',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontWeight: FontWeight.w500,
            fontSize: 20.pf,
            color: marronoscuro,
          ),
        ),
        EspacioVertical(
          height: 20.ph,
        ),
        Icon(
          Icons.sentiment_very_satisfied_outlined,
          size: 60.pf,
          color: marronclarito,
        ),
      ],
    );
  }
}

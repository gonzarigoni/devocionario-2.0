import 'package:devocionario2/constantes.dart';
import 'package:devocionario2/pages/5-finalizacion/bloc/bloc_finalizacion.dart';
import 'package:devocionario2/pages/5-finalizacion/bloc/bloc_finalizacion_state.dart';
import 'package:devocionario2/pages/6-progreso/page_progreso.dart';
import 'package:devocionario2/services/setters.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:full_responsive/full_responsive.dart';
import 'package:intl/intl.dart';

class BotonBottomFinalizacion extends StatelessWidget {
  const BotonBottomFinalizacion({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<BlocFinalizacion, BlocFinalizacionState>(
      builder: (context, state) {
        return SizedBox(
          height: 60.ph,
          width: MediaQuery.of(context).size.width,
          child: Center(
              child: Container(
            width: 175.pw,
            height: 30.ph,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
            ),
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                backgroundColor: marronclarito,
              ),
              onPressed: () async {
                DateTime hoy = DateTime.now();
                String horaDeHoy = DateFormat('HH').format(hoy);
                String fechaAComparar;
                if (int.parse(horaDeHoy) < 5) {
                  fechaAComparar = DateFormat('yyyyMMdd')
                      .format(hoy.subtract(const Duration(days: 1)));
                } else {
                  fechaAComparar = DateFormat('yyyyMMdd').format(hoy);
                }
                if (fechaAComparar != state.ultimoDiaCumplido) {
                  await updateDiasCumplidos();
                  updateDevocion(
                      campo: 'ultimoDiaCumplido', newValue: fechaAComparar);
                }

                Navigator.of(context)
                    .pushReplacement(MaterialPageRoute<PageProgreso>(
                  builder: (BuildContext context) {
                    return const PageProgreso();
                  },
                ));
              },
              child: Text(
                'Terminar',
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: 17.pf,
                ),
              ),
            ),
          )),
        );
      },
    );
  }
}

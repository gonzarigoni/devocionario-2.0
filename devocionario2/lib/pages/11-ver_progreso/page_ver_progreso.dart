import 'package:devocionario2/constantes.dart';
import 'package:devocionario2/pages/2-seleccionador_devocion/page_seleccionador_devocion.dart';
import 'package:devocionario2/pages/6-progreso/bloc/bloc_progreso.dart';
import 'package:devocionario2/pages/6-progreso/bloc/bloc_progreso_event.dart';
import 'package:devocionario2/pages/6-progreso/bloc/bloc_progreso_state.dart';
import 'package:devocionario2/pages/7-promesas/widgets/boton_bottom.dart';
import 'package:devocionario2/pages/6-progreso/widgets/faltan_x_dias.dart';
import 'package:devocionario2/pages/11-ver_progreso/widgets/header_ver_progreso.dart';
import 'package:devocionario2/pages/6-progreso/widgets/llevas_x_dias.dart';
import 'package:devocionario2/pages/6-progreso/widgets/porcentaje.dart';
import 'package:devocionario2/services/setters.dart';
import 'package:devocionario2/widgets_global/appbar_custom.dart';
import 'package:devocionario2/widgets_global/drawer_custom.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:full_responsive/full_responsive.dart';

class PageVerProgreso extends StatefulWidget {
  const PageVerProgreso({super.key});

  @override
  State<PageVerProgreso> createState() => _PageVerProgresoState();
}

class _PageVerProgresoState extends State<PageVerProgreso> {
  @override
  void initState() {
    updatePaginaActual(newValue: 11);
    context.read<BlocProgreso>().add(
        const BlocProgresoGetUserDataEvent(aniosDevocion: 0, diasCumplidos: 0));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<BlocProgreso, BlocProgresoState>(
      builder: (context, state) {
        return Scaffold(
          appBar: const AppBarCustom(),
          drawer: const DrawerCustom(),
          body: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const HeaderVerProgreso(),
              const LlevasXdias(),
              const Porcentaje(),
              const FaltanXdias(),
              EspacioVertical(height: 60.ph)
            ],
          ),
          bottomSheet: ((state.aniosDevocion * 365) != state.diasCumplidos)
              ? const BotonBottom()
              : Container(
                  color: blanquito,
                  height: 60.ph,
                  child: Center(
                    child: SizedBox(
                      height: 30.ph,
                      width: 180.pw,
                      child: OutlinedButton(
                        style: OutlinedButton.styleFrom(
                          side: const BorderSide(
                            width: 2,
                            color: marronclarito,
                          ),
                        ),
                        onPressed: () {
                          Navigator.of(context).pushReplacement(
                              MaterialPageRoute<PageSeleccionadorDevocion>(
                            builder: (BuildContext context) {
                              return const PageSeleccionadorDevocion();
                            },
                          ));
                        },
                        child: Text(
                          'Nueva devoción',
                          style: TextStyle(
                            color: marronclarito,
                            fontSize: 17.pf,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
        );
      },
    );
  }
}

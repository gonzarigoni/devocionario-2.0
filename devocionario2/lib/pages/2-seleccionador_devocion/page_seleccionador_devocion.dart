import 'package:auto_route/auto_route.dart';
import 'package:devocionario2/constantes.dart';
import 'package:devocionario2/pages/3-acceso_a_las_promesas/bloc/bloc_acceso_a_las_promesas.dart';
import 'package:devocionario2/pages/3-acceso_a_las_promesas/bloc/bloc_acceso_a_las_promesas_event.dart';
import 'package:devocionario2/pages/3-acceso_a_las_promesas/page_acceso_a_las_promesas.dart';
import 'package:devocionario2/services/setters.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:full_responsive/full_responsive.dart';

@RoutePage()
class PageSeleccionadorDevocion extends StatefulWidget {
  const PageSeleccionadorDevocion({super.key});

  @override
  State<PageSeleccionadorDevocion> createState() =>
      _PageSeleccionadorDevocionState();
}

class _PageSeleccionadorDevocionState extends State<PageSeleccionadorDevocion> {
  @override
  void initState() {
    super.initState();
    updatePaginaActual(newValue: 2);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: DecoratedBox(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: const AssetImage('assets/splashgris2.png'),
            fit: BoxFit.cover,
            colorFilter: ColorFilter.mode(
              Colors.white.withOpacity(0.6),
              BlendMode.lighten,
            ),
          ),
        ),
        child: Center(
          child: Container(
            decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(10)),
            ),
            width: 350.pw,
            height: 240.ph,
            child: Padding(
              padding: const EdgeInsets.all(10),
              child: Column(
                children: [
                  EspacioVertical(height: 10.ph),
                  Text(
                    'Selecciona la devoción\nque quieras realizar:',
                    style: TextStyle(
                      fontSize: 18.pf,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  EspacioVertical(height: 50.ph),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SizedBox(
                        width: 105.pw,
                        height: 105.ph,
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              backgroundColor: marronclarito,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(15))),
                          onPressed: () {
                            updateDevocion(campo: 'devoTemporal', newValue: 1);
                            context.read<BlocAccesoPromesas>().add(
                                const BlocAccesoPromesasAniosACPEvent(
                                    anios: 1));
                            Navigator.of(context)
                                .push(MaterialPageRoute<PageAccesoPromesas>(
                              builder: (BuildContext context) {
                                return const PageAccesoPromesas();
                              },
                            ));
                          },
                          child: Text(
                            '1\naño',
                            style: TextStyle(
                              fontSize: 30.pf,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                      EspacioHorizontal(width: 30.pw),
                      SizedBox(
                        width: 105.pw,
                        height: 105.ph,
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              backgroundColor: marronclarito,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(15))),
                          onPressed: () {
                            updateDevocion(campo: 'devoTemporal', newValue: 12);
                            context.read<BlocAccesoPromesas>().add(
                                const BlocAccesoPromesasAniosACPEvent(
                                    anios: 12));
                            Navigator.of(context)
                                .push(MaterialPageRoute<PageAccesoPromesas>(
                              builder: (BuildContext context) {
                                return const PageAccesoPromesas();
                              },
                            ));
                          },
                          child: Text(
                            '12\naños',
                            style: TextStyle(
                              fontSize: 30.pf,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

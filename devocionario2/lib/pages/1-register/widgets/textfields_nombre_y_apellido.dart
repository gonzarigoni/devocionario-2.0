import 'package:devocionario2/constantes.dart';
import 'package:devocionario2/pages/1-register/bloc/bloc_register.dart';
import 'package:devocionario2/pages/1-register/bloc/bloc_register_event.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:full_responsive/full_responsive.dart';

class TextFieldsNombreYApellido extends StatelessWidget {
  const TextFieldsNombreYApellido({
    Key? key,
    required this.controllerName,
    required this.controllerLastName,
  }) : super(key: key);
  final TextEditingController controllerName;
  final TextEditingController controllerLastName;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          width: 300.pw,
          child: TextField(
            onChanged: (text) {
              if (controllerName.text.isNotEmpty &&
                  controllerLastName.text.isNotEmpty) {
                context.read<BlocRegister>().add(
                    const BlocRegisterCambiarColorBotonContinuarEvent(
                        isFilled: true));
              } else {
                context.read<BlocRegister>().add(
                    const BlocRegisterCambiarColorBotonContinuarEvent(
                        isFilled: false));
              }
            },
            style: const TextStyle(
              color: marronmedio,
            ),
            controller: controllerName,
            obscureText: false,
            decoration: const InputDecoration(
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(
                  color: negro,
                ),
              ),
              enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(
                  color: negro,
                ),
              ),
              labelText: 'Ingrese Nombre:',
              labelStyle: TextStyle(
                color: fontTextField,
              ),
            ),
          ),
        ),
        EspacioVertical(
          height: 20.ph,
        ),
        SizedBox(
          width: 300.pw,
          child: TextField(
            onChanged: (text) {
              if (controllerName.text.isNotEmpty &&
                  controllerLastName.text.isNotEmpty) {
                context.read<BlocRegister>().add(
                    const BlocRegisterCambiarColorBotonContinuarEvent(
                        isFilled: true));
              } else {
                context.read<BlocRegister>().add(
                    const BlocRegisterCambiarColorBotonContinuarEvent(
                        isFilled: false));
              }
            },
            style: const TextStyle(
              color: marronmedio,
            ),
            controller: controllerLastName,
            obscureText: false,
            decoration: const InputDecoration(
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(
                  color: negro,
                ),
              ),
              enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(
                  color: negro,
                ),
              ),
              labelText: 'Ingrese Apellido:',
              labelStyle: TextStyle(
                color: fontTextField,
              ),
            ),
          ),
        ),
        EspacioVertical(
          height: 10.ph,
        ),
      ],
    );
  }
}

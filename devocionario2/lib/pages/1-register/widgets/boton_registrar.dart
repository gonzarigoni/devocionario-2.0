import 'package:devocionario2/constantes.dart';
import 'package:devocionario2/pages/0-login/page_login.dart';
import 'package:devocionario2/pages/1-register/bloc/bloc_register.dart';
import 'package:devocionario2/pages/1-register/bloc/bloc_register_state.dart';
import 'package:devocionario2/services/auth.dart';
import 'package:devocionario2/widgets_global/dialog_custom.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:full_responsive/full_responsive.dart';

class BotonRegistrar extends StatelessWidget {
  const BotonRegistrar({
    Key? key,
    required this.controllerEmail,
    required this.controllerPass1,
    required this.controllerPass2,
    required this.controllerName,
    required this.controllerLastName,
  }) : super(key: key);
  final TextEditingController controllerEmail;
  final TextEditingController controllerPass1;
  final TextEditingController controllerPass2;
  final TextEditingController controllerName;
  final TextEditingController controllerLastName;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<BlocRegister, BlocRegisterState>(
      builder: (context, state) {
        return Container(
          width: 175.pw,
          height: 30.ph,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
          ),
          child: ElevatedButton(
            style: ElevatedButton.styleFrom(
              backgroundColor:
                  state.isFilledRegister ? marronclarito : disabledColor,
            ),
            onPressed: () async {
              if (state.isFilledRegister) {
                try {
                  final userCredential = await registerUserWithEmailAndPassword(
                    controllerEmail.text,
                    controllerPass1.text,
                  );
                  if (userCredential.user?.uid != null) {
                    final uid = userCredential.user!.uid;
                    await storeUserDetails(
                      uid,
                      controllerName.text,
                      controllerLastName.text,
                      controllerEmail.text,
                    );
                  }
                } catch (e) {
                  showDialog(
                    context: context,
                    builder: (context) => ShowAlertCustom(
                      texto: 'No se pudo registrar el usuario $e',
                      onTap: () => Navigator.of(context).pop,
                    ),
                  );
                }

                Navigator.of(context)
                    .pushReplacement(MaterialPageRoute<PageLogin>(
                  builder: (BuildContext context) {
                    return const PageLogin();
                  },
                ));
              } else {
                showDialog(
                  context: context,
                  builder: (context) => ShowAlertCustom(
                    texto: 'Correo electrónico o contraseña incorrectos',
                    onTap: () => Navigator.of(context).pop,
                  ),
                );
              }
            },
            child: Text(
              'Registrar',
              style: TextStyle(
                fontWeight: FontWeight.w700,
                fontSize: 17.pf,
              ),
            ),
          ),
        );
      },
    );
  }
}

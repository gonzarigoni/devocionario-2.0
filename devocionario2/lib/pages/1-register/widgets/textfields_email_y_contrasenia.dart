// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:devocionario2/constantes.dart';
import 'package:devocionario2/pages/1-register/bloc/bloc_register.dart';
import 'package:devocionario2/pages/1-register/bloc/bloc_register_event.dart';
import 'package:devocionario2/pages/1-register/bloc/bloc_register_state.dart';
import 'package:devocionario2/widgets_global/dialog_custom.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:full_responsive/full_responsive.dart';

class TextFieldsEmailYContrasenia extends StatelessWidget {
  const TextFieldsEmailYContrasenia({
    Key? key,
    required this.controllerEmail,
    required this.controllerPass1,
    required this.controllerPass2,
  }) : super(key: key);
  final TextEditingController controllerEmail;
  final TextEditingController controllerPass1;
  final TextEditingController controllerPass2;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<BlocRegister, BlocRegisterState>(
      builder: (context, state) {
        return Column(children: [
          SizedBox(
            width: 300.pw,
            child: TextField(
              onChanged: (text) {
                if (EmailValidator.validate(controllerEmail.text)) {
                  showDialog(
                    context: context,
                    builder: (context) => ShowAlertCustom(
                      texto: 'Correo electrónico no válido',
                      onTap: () => Navigator.of(context).pop,
                    ),
                  );
                }
                if (controllerPass1.text.isNotEmpty &&
                    controllerPass2.text.isNotEmpty &&
                    controllerPass1.text.compareTo(controllerPass2.text) == 0 &&
                    EmailValidator.validate(controllerEmail.text)) {
                  context.read<BlocRegister>().add(
                      const BlocRegisterCambiarColorBotonRegistrarEvent(
                          isFilledRegister: true));
                } else {
                  context.read<BlocRegister>().add(
                      const BlocRegisterCambiarColorBotonRegistrarEvent(
                          isFilledRegister: false));
                }
              },
              style: const TextStyle(
                color: marronmedio,
              ),
              controller: controllerEmail,
              decoration: const InputDecoration(
                focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(
                    color: negro,
                  ),
                ),
                enabledBorder: UnderlineInputBorder(
                  borderSide: BorderSide(
                    color: negro,
                  ),
                ),
                labelText: 'Ingrese Email:',
                labelStyle: TextStyle(
                  color: fontTextField,
                ),
              ),
            ),
          ),
          EspacioVertical(
            height: 20.ph,
          ),
          SizedBox(
            width: 300.pw,
            child: TextField(
              onChanged: (text) {
                if (controllerPass1.text.isNotEmpty &&
                    controllerPass2.text.isNotEmpty &&
                    controllerPass1.text.compareTo(controllerPass2.text) == 0 &&
                    EmailValidator.validate(controllerEmail.text)) {
                  context.read<BlocRegister>().add(
                      const BlocRegisterCambiarColorBotonRegistrarEvent(
                          isFilledRegister: true));
                } else {
                  context.read<BlocRegister>().add(
                      const BlocRegisterCambiarColorBotonRegistrarEvent(
                          isFilledRegister: false));
                }
              },
              style: const TextStyle(
                color: marronmedio,
              ),
              controller: controllerPass1,
              obscureText: state.isObscurePass,
              decoration: InputDecoration(
                focusedBorder: const UnderlineInputBorder(
                  borderSide: BorderSide(
                    color: negro,
                  ),
                ),
                enabledBorder: const UnderlineInputBorder(
                  borderSide: BorderSide(
                    color: negro,
                  ),
                ),
                labelText: 'Ingrese Contraseña:',
                labelStyle: const TextStyle(
                  color: fontTextField,
                ),
                suffixIcon: IconButton(
                  onPressed: () {
                    context.read<BlocRegister>().add(
                        BlocRegisterObscurePassEvent(
                            isObscurePass: !state.isObscurePass,
                            isObscureRepPass: state.isObscureRepPass));
                  },
                  icon: state.isObscurePass
                      ? const Icon(
                          Icons.visibility_off_outlined,
                          color: marronclarito,
                        )
                      : const Icon(
                          Icons.visibility_outlined,
                          color: marronclarito,
                        ),
                ),
              ),
            ),
          ),
          EspacioVertical(
            height: 20.ph,
          ),
          SizedBox(
            width: 300.pw,
            child: TextField(
              onChanged: (text) {
                if (controllerPass1.text.isNotEmpty &&
                    controllerPass2.text.isNotEmpty &&
                    controllerPass1.text.compareTo(controllerPass2.text) == 0 &&
                    EmailValidator.validate(controllerEmail.text)) {
                  context.read<BlocRegister>().add(
                      const BlocRegisterCambiarColorBotonRegistrarEvent(
                          isFilledRegister: true));
                } else {
                  context.read<BlocRegister>().add(
                      const BlocRegisterCambiarColorBotonRegistrarEvent(
                          isFilledRegister: false));
                }
              },
              style: const TextStyle(
                color: marronmedio,
              ),
              controller: controllerPass2,
              obscureText: state.isObscureRepPass,
              decoration: InputDecoration(
                focusedBorder: const UnderlineInputBorder(
                  borderSide: BorderSide(
                    color: negro,
                  ),
                ),
                enabledBorder: const UnderlineInputBorder(
                  borderSide: BorderSide(
                    color: negro,
                  ),
                ),
                labelText: 'Repetir Contraseña:',
                labelStyle: const TextStyle(
                  color: fontTextField,
                ),
                suffixIcon: IconButton(
                  onPressed: () {
                    if (controllerPass1.text.compareTo(controllerPass2.text) ==
                        0) {
                      showDialog(
                        context: context,
                        builder: (context) => ShowAlertCustom(
                          texto: 'Las contraseñas no coinciden',
                          onTap: () => Navigator.of(context).pop,
                        ),
                      );
                    }
                    context.read<BlocRegister>().add(
                        BlocRegisterObscurePassEvent(
                            isObscurePass: state.isObscurePass,
                            isObscureRepPass: !state.isObscureRepPass));
                  },
                  icon: state.isObscureRepPass
                      ? const Icon(
                          Icons.visibility_off_outlined,
                          color: marronclarito,
                        )
                      : const Icon(
                          Icons.visibility_outlined,
                          color: marronclarito,
                        ),
                ),
              ),
            ),
          ),
          EspacioVertical(
            height: 10.ph,
          ),
          state.isRegistro1
              ? GestureDetector(
                  onTap: () {
                    //TODO popups
                  },
                  child: SizedBox(
                    width: 300.pw,
                    child: Text(
                      'Las contraseñas no coinciden',
                      style: TextStyle(
                          fontWeight: FontWeight.w400,
                          fontSize: 16.pf,
                          color: marronclarito),
                      textAlign: TextAlign.end,
                    ),
                  ),
                )
              : Container()
        ]);
      },
    );
  }
}

import 'package:devocionario2/constantes.dart';
import 'package:devocionario2/pages/0-login/page_login.dart';
import 'package:flutter/material.dart';
import 'package:full_responsive/full_responsive.dart';

class YaEstoyRegistrado extends StatelessWidget {
  const YaEstoyRegistrado({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).pushReplacement(MaterialPageRoute<PageLogin>(
          builder: (BuildContext context) {
            return const PageLogin();
          },
        ));
      },
      child: SizedBox(
        width: 300.pw,
        child: RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
            style: TextStyle(
              fontSize: 16.pf,
            ),
            children: const [
              TextSpan(
                text: '¡Ya estoy ',
                style: TextStyle(
                  color: marronclarito,
                  fontWeight: FontWeight.w700,
                ),
              ),
              TextSpan(
                text: 'registrado',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: marronmedio,
                ),
              ),
              TextSpan(
                text: '!',
                style: TextStyle(
                  color: marronclarito,
                  fontWeight: FontWeight.w700,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

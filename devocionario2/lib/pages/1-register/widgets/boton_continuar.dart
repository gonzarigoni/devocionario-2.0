import 'package:devocionario2/constantes.dart';
import 'package:devocionario2/pages/1-register/bloc/bloc_register.dart';
import 'package:devocionario2/pages/1-register/bloc/bloc_register_event.dart';
import 'package:devocionario2/pages/1-register/bloc/bloc_register_state.dart';
import 'package:devocionario2/widgets_global/dialog_custom.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:full_responsive/full_responsive.dart';

class BotonContinuar extends StatelessWidget {
  const BotonContinuar({
    Key? key,
    required this.controllerName,
    required this.controllerLastName,
  }) : super(key: key);
  final TextEditingController controllerName;
  final TextEditingController controllerLastName;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<BlocRegister, BlocRegisterState>(
      builder: (context, state) {
        return Container(
          width: 175.pw,
          height: 30.ph,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
          ),
          child: ElevatedButton(
            style: ElevatedButton.styleFrom(
              backgroundColor: state.isFilled ? marronclarito : disabledColor,
            ),
            onPressed: () async {
              if (state.isFilled) {
                context.read<BlocRegister>().add(
                    const BlocRegisterCambiarTextFieldsEvent(
                        isRegistro1: false));
              } else {
                showDialog(
                  context: context,
                  builder: (context) => ShowAlertCustom(
                    texto: 'Debes completar todos los campos',
                    onTap: () => Navigator.of(context).pop,
                  ),
                );
              }
            },
            child: Text(
              'Continuar',
              style: TextStyle(
                fontWeight: FontWeight.w700,
                fontSize: 17.pf,
              ),
            ),
          ),
        );
      },
    );
  }
}

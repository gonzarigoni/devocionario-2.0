import 'package:devocionario2/constantes.dart';
import 'package:flutter/material.dart';
import 'package:full_responsive/full_responsive.dart';

class TextosBienvenida extends StatelessWidget {
  const TextosBienvenida({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          '¡Bienvenido!',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontWeight: FontWeight.w700,
            fontSize: 40.pf,
            color: marronmedio,
          ),
        ),
        EspacioVertical(
          height: 20.ph,
        ),
        Text(
          'Registrate para disfrutar tus devociones',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontWeight: FontWeight.w500,
            fontSize: 20.pf,
            color: marronoscuro,
          ),
        ),
        EspacioVertical(
          height: 20.ph,
        ),
        Icon(
          Icons.sentiment_very_satisfied_outlined,
          size: 40.pf,
          color: marronmedio,
        ),
      ],
    );
  }
}

// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:equatable/equatable.dart';

class BlocRegisterState extends Equatable {
  const BlocRegisterState({
    required this.isFilled,
    required this.isRegistro1,
    required this.isFilledRegister,
    required this.isObscurePass,
    required this.isObscureRepPass,
  });

  final bool isFilled;
  final bool isRegistro1;
  final bool isFilledRegister;
  final bool isObscurePass;
  final bool isObscureRepPass;
  @override
  List<Object> get props => [
        isFilled,
        isRegistro1,
        isFilledRegister,
        isObscurePass,
        isObscureRepPass
      ];

  BlocRegisterState copyWith({
    bool? isFilled,
    bool? isRegistro1,
    bool? isFilledRegister,
    bool? isObscurePass,
    bool? isObscureRepPass,
  }) {
    return BlocRegisterState(
      isFilled: isFilled ?? this.isFilled,
      isRegistro1: isRegistro1 ?? this.isRegistro1,
      isFilledRegister: isFilledRegister ?? this.isFilledRegister,
      isObscurePass: isObscurePass ?? this.isObscurePass,
      isObscureRepPass: isObscureRepPass ?? this.isObscureRepPass,
    );
  }
}

class RegisterInitial extends BlocRegisterState {
  const RegisterInitial({
    required bool isFilled,
    required bool isRegistro1,
    required bool isFilledRegister,
    required bool isObscurePass,
    required bool isObscureRepPass,
  }) : super(
          isFilled: isFilled,
          isRegistro1: isRegistro1,
          isFilledRegister: isFilledRegister,
          isObscurePass: isObscurePass,
          isObscureRepPass: isObscureRepPass,
        );
}

// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:equatable/equatable.dart';

abstract class BlocRegisterEvent extends Equatable {
  const BlocRegisterEvent();

  @override
  List<Object> get props => [];
}

class BlocRegisterCambiarColorBotonContinuarEvent extends BlocRegisterEvent {
  const BlocRegisterCambiarColorBotonContinuarEvent({
    required this.isFilled,
  });
  final bool isFilled;
}

class BlocRegisterCambiarColorBotonRegistrarEvent extends BlocRegisterEvent {
  const BlocRegisterCambiarColorBotonRegistrarEvent({
    required this.isFilledRegister,
  });
  final bool isFilledRegister;
}

class BlocRegisterCambiarTextFieldsEvent extends BlocRegisterEvent {
  const BlocRegisterCambiarTextFieldsEvent({
    required this.isRegistro1,
  });
  final bool isRegistro1;
}

class BlocRegisterObscurePassEvent extends BlocRegisterEvent {
  const BlocRegisterObscurePassEvent({
    required this.isObscurePass,
    required this.isObscureRepPass,
  });
  final bool isObscurePass;
  final bool isObscureRepPass;
}

import 'dart:async';

import 'package:devocionario2/pages/1-register/bloc/bloc_register_event.dart';
import 'package:devocionario2/pages/1-register/bloc/bloc_register_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class BlocRegister extends Bloc<BlocRegisterEvent, BlocRegisterState> {
  BlocRegister()
      : super(const RegisterInitial(
          isFilled: false,
          isRegistro1: true,
          isFilledRegister: false,
          isObscurePass: true,
          isObscureRepPass: true,
        )) {
    on<BlocRegisterCambiarColorBotonContinuarEvent>(
        _cambiarColorBotonContinuar);
    on<BlocRegisterCambiarColorBotonRegistrarEvent>(
        _cambiarColorBotonRegistrar);
    on<BlocRegisterCambiarTextFieldsEvent>(_cambiarTextFields);
    on<BlocRegisterObscurePassEvent>(_obscurePass);
  }

  FutureOr<void> _cambiarColorBotonContinuar(
      BlocRegisterCambiarColorBotonContinuarEvent event,
      Emitter<BlocRegisterState> emit) {
    try {
      final bool isFilled = event.isFilled;
      emit(
        state.copyWith(isFilled: isFilled),
      );
    } catch (e) {
      print(e);
    }
  }

  FutureOr<void> _cambiarColorBotonRegistrar(
      BlocRegisterCambiarColorBotonRegistrarEvent event,
      Emitter<BlocRegisterState> emit) {
    try {
      final bool isFilledRegister = event.isFilledRegister;
      emit(
        state.copyWith(isFilledRegister: isFilledRegister),
      );
    } catch (e) {
      print(e);
    }
  }

  FutureOr<void> _cambiarTextFields(BlocRegisterCambiarTextFieldsEvent event,
      Emitter<BlocRegisterState> emit) {
    try {
      final bool isRegistro1 = event.isRegistro1;
      emit(
        state.copyWith(isRegistro1: isRegistro1),
      );
    } catch (e) {
      print(e);
    }
  }

  FutureOr<void> _obscurePass(
      BlocRegisterObscurePassEvent event, Emitter<BlocRegisterState> emit) {
    try {
      final bool isObscurePass = event.isObscurePass;
      final bool isObscureRepPass = event.isObscureRepPass;
      emit(
        state.copyWith(
            isObscurePass: isObscurePass, isObscureRepPass: isObscureRepPass),
      );
    } catch (e) {
      print(e);
    }
  }
}

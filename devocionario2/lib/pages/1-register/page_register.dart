import 'package:auto_route/auto_route.dart';
import 'package:devocionario2/constantes.dart';
import 'package:devocionario2/pages/1-register/bloc/bloc_register.dart';
import 'package:devocionario2/pages/1-register/bloc/bloc_register_event.dart';
import 'package:devocionario2/pages/1-register/bloc/bloc_register_state.dart';
import 'package:devocionario2/pages/1-register/widgets/boton_continuar.dart';
import 'package:devocionario2/pages/1-register/widgets/boton_registrar.dart';
import 'package:devocionario2/pages/1-register/widgets/textfields_email_y_contrasenia.dart';
import 'package:devocionario2/pages/1-register/widgets/textfields_nombre_y_apellido.dart';
import 'package:devocionario2/pages/1-register/widgets/texto_bienvenida_registrar.dart';
import 'package:devocionario2/pages/1-register/widgets/ya_estoy_registrado.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

@RoutePage()
class PageRegister extends StatefulWidget {
  const PageRegister({super.key});

  @override
  State<PageRegister> createState() => _PageRegisterState();
}

class _PageRegisterState extends State<PageRegister> {
  final TextEditingController controllerName = TextEditingController();

  final TextEditingController controllerLastName = TextEditingController();

  final TextEditingController controllerEmail = TextEditingController();

  final TextEditingController controllerPass1 = TextEditingController();

  final TextEditingController controllerPass2 = TextEditingController();
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          leading: BlocBuilder<BlocRegister, BlocRegisterState>(
            builder: (context, state) {
              return state.isRegistro1
                  ? Container()
                  : GestureDetector(
                      onTap: () async {
                        context.read<BlocRegister>().add(
                            const BlocRegisterCambiarTextFieldsEvent(
                                isRegistro1: true));
                      },
                      child: const Icon(
                        Icons.arrow_back,
                        color: marronoscuro,
                      ),
                    );
            },
          ),
          backgroundColor: Colors.transparent,
          elevation: 0,
        ),
        body: SizedBox(
          width: MediaQuery.of(context).size.width,
          child: BlocBuilder<BlocRegister, BlocRegisterState>(
            builder: (context, state) {
              return Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  const TextosBienvenida(),
                  state.isRegistro1
                      ? TextFieldsNombreYApellido(
                          controllerName: controllerName,
                          controllerLastName: controllerLastName)
                      : TextFieldsEmailYContrasenia(
                          controllerEmail: controllerEmail,
                          controllerPass1: controllerPass1,
                          controllerPass2: controllerPass2),
                  state.isRegistro1
                      ? BotonContinuar(
                          controllerName: controllerName,
                          controllerLastName: controllerLastName)
                      : BotonRegistrar(
                          controllerLastName: controllerLastName,
                          controllerName: controllerName,
                          controllerEmail: controllerEmail,
                          controllerPass1: controllerPass1,
                          controllerPass2: controllerPass2),
                  state.isRegistro1 ? const YaEstoyRegistrado() : Container(),
                ],
              );
            },
          ),
        ),
      ),
    );
  }
}

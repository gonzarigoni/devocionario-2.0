import 'package:auto_route/auto_route.dart';
import 'package:devocionario2/constantes.dart';
import 'package:devocionario2/pages/9-configuracion/bloc/bloc_configuracion.dart';
import 'package:devocionario2/pages/9-configuracion/bloc/bloc_configuracion_event.dart';
import 'package:devocionario2/pages/9-configuracion/bloc/bloc_configuracion_state.dart';
import 'package:devocionario2/services/setters.dart';
import 'package:devocionario2/widgets_global/dialog_custom.dart';
import 'package:devocionario2/widgets_global/drawer_custom.dart';
import 'package:devocionario2/widgets_global/appbar_custom.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:full_responsive/full_responsive.dart';

@RoutePage()
class PageConfiguracion extends StatefulWidget {
  const PageConfiguracion({super.key});

  @override
  State<PageConfiguracion> createState() => _PageConfiguracionState();
}

class _PageConfiguracionState extends State<PageConfiguracion> {
  @override
  void initState() {
    updatePaginaActual(newValue: 9);
    context
        .read<BlocConfiguracion>()
        .add(const BlocConfiguracionGetUserDataEvent(
          nombre: '',
          apellido: '',
          email: '',
        ));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<BlocConfiguracion, BlocConfiguracionState>(
      builder: (context, state) {
        if (state.status == BlocConfiguracionStatus.loading) {
          return const Scaffold(
            body: Center(
                child: CircularProgressIndicator(
              color: marronmedio,
              backgroundColor: Color(0xffdddddd),
            )),
          );
        }
        if (state.status == BlocConfiguracionStatus.success) {
          return Scaffold(
              appBar: const AppBarCustom(),
              drawer: const DrawerCustom(),
              body: SizedBox(
                width: MediaQuery.of(context).size.width,
                child: Column(
                  children: [
                    Text(
                      'Configuración',
                      style: TextStyle(
                        fontSize: 23.pf,
                        color: marronclarito,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Activar notificaciones',
                          style: TextStyle(
                            fontSize: 18.pf,
                            color: marronclarito,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        Switch(
                          value: true,
                          onChanged: (value) {},
                        )
                      ],
                    ),
                    ListView.builder(
                      itemCount: 5,
                      shrinkWrap: true,
                      itemBuilder: (context, index) {
                        return Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Row(
                              children: [
                                Checkbox(
                                  value: true,
                                  onChanged: (value) {},
                                ),
                                const Text('Primer alarma')
                              ],
                            ),
                            Switch(
                              value: true,
                              onChanged: (value) {},
                            )
                          ],
                        );
                      },
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        TextButton.icon(
                            onPressed: () {},
                            icon: const Icon(Icons.plus_one),
                            label: const Text('Agregar notificacion')),
                        GestureDetector(
                          onTap: () {},
                          child: const Icon(Icons.delete_outlined),
                        )
                      ],
                    )
                  ],
                ),
              ));
        }
        if (state.status == BlocConfiguracionStatus.error) {
          showDialog(
            context: context,
            builder: (context) => ShowAlertCustom(
              texto: 'Error al obtener los datos del usuario',
              onTap: () async {
                Navigator.of(context).pop();
              },
            ),
          );
        }
        return Container();
      },
    );
  }
}

// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:equatable/equatable.dart';

enum BlocConfiguracionStatus {
  /// estado inicial sin nada cargado
  initial,

  /// estado cargado
  success,

  /// estado de error
  error,

  /// estado cargando...
  loading;

  ///devuleve true si el estado es Success
  bool get isSuccess => this == BlocConfiguracionStatus.success;

  ///devuelve true si el estado es Error
  bool get isError => this == BlocConfiguracionStatus.error;

  ///devuleve true si el estado es Loading
  bool get isLoading => this == BlocConfiguracionStatus.loading;

  ///devuelve un estado de true al iniciar
  bool get isInitial => this == BlocConfiguracionStatus.initial;
}

class BlocConfiguracionState extends Equatable {
  const BlocConfiguracionState({
    required this.nombre,
    required this.apellido,
    required this.email,
    required this.status,
  });

  final String nombre;
  final String apellido;
  final String email;
  final BlocConfiguracionStatus status;

  @override
  List<Object> get props => [
        apellido,
        nombre,
        email,
        status,
      ];

  BlocConfiguracionState copyWith({
    final String? nombre,
    final String? apellido,
    final String? email,
    final BlocConfiguracionStatus? status,
  }) {
    return BlocConfiguracionState(
      nombre: nombre ?? this.nombre,
      apellido: apellido ?? this.apellido,
      email: email ?? this.email,
      status: status ?? this.status,
    );
  }
}

class ConfiguracionInitial extends BlocConfiguracionState {
  const ConfiguracionInitial({
    required String nombre,
    required String apellido,
    required String email,
    required BlocConfiguracionStatus status,
  }) : super(
          nombre: nombre,
          apellido: apellido,
          email: email,
          status: BlocConfiguracionStatus.initial,
        );
}

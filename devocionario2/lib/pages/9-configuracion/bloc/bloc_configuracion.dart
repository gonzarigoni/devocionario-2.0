import 'dart:async';
import 'package:devocionario2/pages/9-configuracion/bloc/bloc_configuracion_event.dart';
import 'package:devocionario2/pages/9-configuracion/bloc/bloc_configuracion_state.dart';
import 'package:devocionario2/services/getters.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class BlocConfiguracion
    extends Bloc<BlocConfiguracionEvent, BlocConfiguracionState> {
  BlocConfiguracion()
      : super(const ConfiguracionInitial(
          nombre: '',
          apellido: '',
          email: '',
          status: BlocConfiguracionStatus.initial,
        )) {
    on<BlocConfiguracionGetUserDataEvent>(_getUserData);
  }

  FutureOr<void> _getUserData(BlocConfiguracionGetUserDataEvent event,
      Emitter<BlocConfiguracionState> emit) async {
    emit(state.copyWith(status: BlocConfiguracionStatus.loading));
    try {
      Map<String, dynamic> response = await getUserDataProfile();
      final String? nombre = response['nombre'];
      final String? apellido = response['apellido'];
      final String? email = response['email'];

      emit(
        state.copyWith(
          nombre: nombre,
          apellido: apellido,
          email: email,
          status: BlocConfiguracionStatus.success,
        ),
      );
    } catch (e) {
      emit(state.copyWith(status: BlocConfiguracionStatus.error));
    }
  }
}

// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:equatable/equatable.dart';

abstract class BlocConfiguracionEvent extends Equatable {
  const BlocConfiguracionEvent();

  @override
  List<Object> get props => [];
}

class BlocConfiguracionGetUserDataEvent extends BlocConfiguracionEvent {
  const BlocConfiguracionGetUserDataEvent({
    required this.nombre,
    required this.apellido,
    required this.email,
  });
  final String? nombre;
  final String? apellido;
  final String? email;
}

import 'dart:async';
import 'package:devocionario2/pages/3-acceso_a_las_promesas/bloc/bloc_acceso_a_las_promesas_event.dart';
import 'package:devocionario2/pages/3-acceso_a_las_promesas/bloc/bloc_acceso_a_las_promesas_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class BlocAccesoPromesas
    extends Bloc<BlocAccesoPromesasEvent, BlocAccesoPromesasState> {
  BlocAccesoPromesas()
      : super(AccesoPromesasInitial(
          fecha: DateTime.now(),
          isObscure0: false,
          isObscure1: false,
          isObscure2: false,
          anios: 0,
        )) {
    on<BlocAccesoPromesasCambiarFechaEvent>(_cambiarFecha);
    on<BlocAccesoPromesasObscureEvent>(_cambiarObscures);
    on<BlocAccesoPromesasAniosACPEvent>(_aniosParaAccesoALasPromesas);
  }

  Future<void> _cambiarFecha(
    BlocAccesoPromesasCambiarFechaEvent event,
    Emitter<BlocAccesoPromesasState> emit,
  ) async {
    try {
      final DateTime? fecha = event.fecha;
      emit(state.copyWith(fecha: fecha));
    } catch (e) {
      print(e);
    }
  }

  FutureOr<void> _cambiarObscures(BlocAccesoPromesasObscureEvent event,
      Emitter<BlocAccesoPromesasState> emit) {
    try {
      final bool isObscure0 = event.isObscure0;
      final bool isObscure1 = event.isObscure1;
      final bool isObscure2 = event.isObscure2;
      emit(
        state.copyWith(
          isObscure0: isObscure0,
          isObscure1: isObscure1,
          isObscure2: isObscure2,
        ),
      );
    } catch (e) {
      print(e);
    }
  }

  FutureOr<void> _aniosParaAccesoALasPromesas(
      BlocAccesoPromesasAniosACPEvent event,
      Emitter<BlocAccesoPromesasState> emit) async {
    try {
      int? anios = event.anios;
      emit(
        state.copyWith(anios: anios),
      );
    } catch (e) {
      print(e);
    }
  }
}

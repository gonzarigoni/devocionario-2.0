// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:equatable/equatable.dart';

class BlocAccesoPromesasState extends Equatable {
  const BlocAccesoPromesasState(
      {required this.fecha,
      required this.isObscure0,
      required this.isObscure1,
      required this.isObscure2,
      required this.anios});

  final DateTime fecha;
  final bool isObscure0;
  final bool isObscure1;
  final bool isObscure2;
  final int anios;
  @override
  List<Object> get props => [
        fecha,
        isObscure0,
        isObscure1,
        isObscure2,
        anios,
      ];

  BlocAccesoPromesasState copyWith({
    DateTime? fecha,
    bool? isObscure0,
    bool? isObscure1,
    bool? isObscure2,
    int? anios,
  }) {
    return BlocAccesoPromesasState(
        fecha: fecha ?? this.fecha,
        isObscure0: isObscure0 ?? this.isObscure0,
        isObscure1: isObscure1 ?? this.isObscure1,
        isObscure2: isObscure2 ?? this.isObscure2,
        anios: anios ?? this.anios);
  }
}

class AccesoPromesasInitial extends BlocAccesoPromesasState {
  const AccesoPromesasInitial({
    required DateTime fecha,
    required bool isObscure0,
    required bool isObscure1,
    required bool isObscure2,
    required int anios,
  }) : super(
            fecha: fecha,
            isObscure0: isObscure0,
            isObscure1: isObscure1,
            isObscure2: isObscure2,
            anios: anios);
}

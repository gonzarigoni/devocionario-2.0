// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:equatable/equatable.dart';

abstract class BlocAccesoPromesasEvent extends Equatable {
  const BlocAccesoPromesasEvent();

  @override
  List<Object> get props => [];
}

class BlocAccesoPromesasCambiarFechaEvent extends BlocAccesoPromesasEvent {
  const BlocAccesoPromesasCambiarFechaEvent({
    required this.fecha,
  });
  final DateTime? fecha;
}

class BlocAccesoPromesasObscureEvent extends BlocAccesoPromesasEvent {
  const BlocAccesoPromesasObscureEvent({
    required this.isObscure0,
    required this.isObscure1,
    required this.isObscure2,
  });
  final bool isObscure0;
  final bool isObscure1;
  final bool isObscure2;
}

class BlocAccesoPromesasAniosACPEvent extends BlocAccesoPromesasEvent {
  const BlocAccesoPromesasAniosACPEvent({
    required this.anios,
  });
  final int? anios;
}

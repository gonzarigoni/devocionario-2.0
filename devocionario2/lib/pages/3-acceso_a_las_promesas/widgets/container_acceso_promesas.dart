import 'package:devocionario2/constantes.dart';
import 'package:devocionario2/pages/3-acceso_a_las_promesas/widgets/personas_elegidas.dart';
import 'package:flutter/material.dart';
import 'package:full_responsive/full_responsive.dart';
import 'package:timelines/timelines.dart';

class ContainerAccesoPromesas extends StatelessWidget {
  const ContainerAccesoPromesas({
    super.key,
    required this.aniosDevocion,
    required this.controller0,
    required this.controller1,
    required this.controller2,
  });

  final int aniosDevocion;
  final TextEditingController controller0;
  final TextEditingController controller1;
  final TextEditingController controller2;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: ClipRRect(
        borderRadius: const BorderRadius.only(
          topLeft: Radius.circular(20),
          topRight: Radius.circular(20),
        ),
        child: SingleChildScrollView(
          child: Container(
            color: blanquito,
            child: Padding(
              padding: EdgeInsets.only(left: 20.pw, top: 15.ph),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.only(left: 35.pw),
                    child: Text(
                      'Promesas',
                      style: TextStyle(
                        color: marronoscuro,
                        fontSize: 23.pf,
                        fontWeight: FontWeight.w700,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(15),
                    child: FixedTimeline.tileBuilder(
                      theme: TimelineThemeData(
                        color: marronclarito,
                      ),
                      builder: TimelineTileBuilder.connectedFromStyle(
                        contentsAlign: ContentsAlign.reverse,
                        oppositeContentsBuilder: (context, index) => Padding(
                          padding: const EdgeInsets.only(left: 8, bottom: 20),
                          child: Column(
                            children: [
                              Text(
                                aniosDevocion == 12
                                    ? objPromesas12[index]!
                                    : objPromesas1[index]!,
                                style: const TextStyle(
                                  color: marronoscuro,
                                ),
                              ),
                              if (index == 2 && aniosDevocion == 12)
                                PersonasElegidasTextFields(
                                    controller0: controller0,
                                    controller1: controller1,
                                    controller2: controller2),
                            ],
                          ),
                        ),
                        itemCount: aniosDevocion == 12 ? 5 : 21,
                        nodePositionBuilder: (context, index) => 0,
                        indicatorPositionBuilder: (context, index) => 0,
                        connectorStyleBuilder: (context, index) =>
                            ConnectorStyle.solidLine,
                        lastConnectorStyle: ConnectorStyle.transparent,
                        indicatorStyleBuilder: (context, index) =>
                            IndicatorStyle.dot,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

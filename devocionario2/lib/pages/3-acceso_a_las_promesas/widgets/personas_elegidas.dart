import 'package:devocionario2/constantes.dart';
import 'package:devocionario2/pages/3-acceso_a_las_promesas/bloc/bloc_acceso_a_las_promesas.dart';
import 'package:devocionario2/pages/3-acceso_a_las_promesas/bloc/bloc_acceso_a_las_promesas_event.dart';
import 'package:devocionario2/pages/3-acceso_a_las_promesas/bloc/bloc_acceso_a_las_promesas_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PersonasElegidasTextFields extends StatelessWidget {
  const PersonasElegidasTextFields({
    super.key,
    required this.controller0,
    required this.controller1,
    required this.controller2,
  });

  final TextEditingController controller0;
  final TextEditingController controller1;
  final TextEditingController controller2;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<BlocAccesoPromesas, BlocAccesoPromesasState>(
      builder: (context, state) {
        return Padding(
          padding: const EdgeInsets.only(top: 10),
          child: ConstrainedBox(
            constraints: const BoxConstraints(maxWidth: 400),
            child: Column(
              children: [
                SizedBox(
                  height: 40,
                  child: TextField(
                    maxLength: 50,
                    style: const TextStyle(color: marronmedio),
                    obscureText: state.isObscure0,
                    decoration: InputDecoration(
                      enabledBorder: const OutlineInputBorder(
                        borderSide: BorderSide(
                          color: marronmedio,
                        ),
                      ),
                      focusedBorder: const OutlineInputBorder(
                        borderSide: BorderSide(
                          color: marronmedio,
                          width: 2,
                        ),
                      ),
                      hintStyle: const TextStyle(
                        fontSize: 15,
                        color: marronmedio,
                      ),
                      counterText: '',
                      contentPadding: const EdgeInsets.symmetric(
                          vertical: 5, horizontal: 10),
                      hintText: 'Escriba aquí',
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5),
                      ),
                      filled: true,
                      fillColor: Colors.white,
                      suffixIcon: IconButton(
                        icon: Icon(
                          state.isObscure0
                              ? Icons.visibility_off_outlined
                              : Icons.visibility_outlined,
                          color: marronclarito,
                        ),
                        onPressed: () {
                          context.read<BlocAccesoPromesas>().add(
                                BlocAccesoPromesasObscureEvent(
                                  isObscure0: !state.isObscure0,
                                  isObscure1: state.isObscure1,
                                  isObscure2: state.isObscure2,
                                ),
                              );
                        },
                      ),
                    ),
                    controller: controller0,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(0, 8, 0, 8),
                  child: SizedBox(
                    height: 40,
                    child: TextField(
                      maxLength: 40,
                      style: const TextStyle(color: marronmedio),
                      obscureText: state.isObscure1,
                      decoration: InputDecoration(
                        enabledBorder: const OutlineInputBorder(
                          borderSide: BorderSide(
                            color: marronmedio,
                          ),
                        ),
                        focusedBorder: const OutlineInputBorder(
                          borderSide: BorderSide(
                            color: marronmedio,
                            width: 2,
                          ),
                        ),
                        hintStyle: const TextStyle(
                          fontSize: 15,
                          color: marronmedio,
                        ),
                        counterText: '',
                        contentPadding: const EdgeInsets.symmetric(
                            vertical: 5, horizontal: 10),
                        hintText: 'Escriba aquí',
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5),
                        ),
                        filled: true,
                        fillColor: Colors.white,
                        suffixIcon: IconButton(
                          icon: Icon(
                            state.isObscure1
                                ? Icons.visibility_off_outlined
                                : Icons.visibility_outlined,
                            color: marronclarito,
                          ),
                          onPressed: () {
                            context
                                .read<BlocAccesoPromesas>()
                                .add(BlocAccesoPromesasObscureEvent(
                                  isObscure0: state.isObscure0,
                                  isObscure1: !state.isObscure1,
                                  isObscure2: state.isObscure2,
                                ));
                          },
                        ),
                      ),
                      controller: controller1,
                    ),
                  ),
                ),
                SizedBox(
                  height: 40,
                  child: TextField(
                    maxLength: 40,
                    style: const TextStyle(color: marronmedio),
                    obscureText: state.isObscure2,
                    decoration: InputDecoration(
                      enabledBorder: const OutlineInputBorder(
                        borderSide: BorderSide(
                          color: marronmedio,
                        ),
                      ),
                      focusedBorder: const OutlineInputBorder(
                        borderSide: BorderSide(
                          color: marronmedio,
                          width: 2,
                        ),
                      ),
                      hintStyle: const TextStyle(
                        fontSize: 15,
                        color: marronmedio,
                      ),
                      counterText: '',
                      contentPadding: const EdgeInsets.symmetric(
                          vertical: 5, horizontal: 10),
                      hintText: 'Escriba aquí',
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5),
                      ),
                      filled: true,
                      fillColor: Colors.white,
                      suffixIcon: IconButton(
                        icon: Icon(
                          state.isObscure2
                              ? Icons.visibility_off_outlined
                              : Icons.visibility_outlined,
                          color: marronclarito,
                        ),
                        onPressed: () {
                          context.read<BlocAccesoPromesas>().add(
                                BlocAccesoPromesasObscureEvent(
                                  isObscure0: state.isObscure0,
                                  isObscure1: state.isObscure1,
                                  isObscure2: !state.isObscure2,
                                ),
                              );
                        },
                      ),
                    ),
                    controller: controller2,
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}

import 'package:devocionario2/constantes.dart';
import 'package:flutter/material.dart';
import 'package:full_responsive/full_responsive.dart';

class Header extends StatelessWidget {
  const Header({
    super.key,
    required this.aniosDevocion,
  });

  final int aniosDevocion;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          'Sta. Brígida de $aniosDevocion ${aniosDevocion == 1 ? 'año' : 'años'}',
          style: TextStyle(
            color: marronoscuro,
            fontSize: 23.pf,
            fontWeight: FontWeight.w700,
          ),
        ),
        EspacioVertical(height: 5.ph),
        Text(
          'Fecha de comienzo',
          style: TextStyle(
            color: marronoscuro,
            fontSize: 14.pf,
            fontWeight: FontWeight.w600,
          ),
        ),
      ],
    );
  }
}

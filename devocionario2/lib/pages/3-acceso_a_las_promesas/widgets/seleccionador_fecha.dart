import 'package:devocionario2/constantes.dart';
import 'package:devocionario2/pages/3-acceso_a_las_promesas/bloc/bloc_acceso_a_las_promesas.dart';
import 'package:devocionario2/pages/3-acceso_a_las_promesas/bloc/bloc_acceso_a_las_promesas_event.dart';
import 'package:devocionario2/pages/3-acceso_a_las_promesas/bloc/bloc_acceso_a_las_promesas_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:full_responsive/full_responsive.dart';

class SeleccionadorFecha extends StatelessWidget {
  const SeleccionadorFecha({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<BlocAccesoPromesas, BlocAccesoPromesasState>(
      builder: (context, state) {
        return GestureDetector(
          onTap: () async {
            context.read<BlocAccesoPromesas>().add(
                BlocAccesoPromesasCambiarFechaEvent(
                    fecha: await showDatePicker(
                            context: context,
                            initialDate: state.fecha,
                            firstDate: DateTime(2010),
                            lastDate: DateTime.now()) ??
                        state.fecha));
          },
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
              color: blanquito,
            ),
            width: 220.pw,
            height: 35.ph,
            child: Center(
              child: Text(
                '${state.fecha.day}/${state.fecha.month}/${state.fecha.year}',
                style: TextStyle(
                  color: fontColorDisabled,
                  fontSize: 16.pf,
                  fontWeight: FontWeight.w400,
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}

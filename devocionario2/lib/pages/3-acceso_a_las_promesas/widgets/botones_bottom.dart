import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:devocionario2/constantes.dart';
import 'package:devocionario2/pages/2-seleccionador_devocion/page_seleccionador_devocion.dart';
import 'package:devocionario2/pages/3-acceso_a_las_promesas/bloc/bloc_acceso_a_las_promesas.dart';
import 'package:devocionario2/pages/3-acceso_a_las_promesas/bloc/bloc_acceso_a_las_promesas_state.dart';
import 'package:devocionario2/pages/4-oraciones/page_oraciones.dart';
import 'package:devocionario2/services/setters.dart';
import 'package:devocionario2/widgets_global/dialog_custom.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:full_responsive/full_responsive.dart';

class BotonesBottom extends StatelessWidget {
  const BotonesBottom({
    super.key,
    required this.aniosDevocion,
    required this.controller0,
    required this.controller1,
    required this.controller2,
  });

  final int aniosDevocion;
  final TextEditingController controller0;
  final TextEditingController controller1;
  final TextEditingController controller2;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<BlocAccesoPromesas, BlocAccesoPromesasState>(
      builder: (context, state) {
        return Container(
          color: blanquito,
          height: 60.ph,
          child: Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                SizedBox(
                  height: 30.ph,
                  width: 120.pw,
                  child: OutlinedButton(
                    style: OutlinedButton.styleFrom(
                      side: const BorderSide(
                        width: 2,
                        color: marronclarito,
                      ),
                    ),
                    onPressed: () {
                      Navigator.of(context).pushReplacement(
                          MaterialPageRoute<PageSeleccionadorDevocion>(
                        builder: (BuildContext context) {
                          return const PageSeleccionadorDevocion();
                        },
                      ));
                    },
                    child: Text(
                      'Atrás',
                      style: TextStyle(
                        color: marronclarito,
                        fontSize: 17.pf,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 30.ph,
                  width: 120.pw,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: enabledColor,
                    ),
                    onPressed: () {
                      showDialog(
                        context: context,
                        builder: (context) => ShowAlertCustom(
                          texto:
                              'Está a punto de continuar, revise la información detalladamente ya que no podrá modificarse',
                          onTap: () async {
                            await updateUserSettings(
                              devocion: aniosDevocion,
                              diasCumplidos: (DateTime.now()
                                  .difference(state.fecha)
                                  .inDays),
                              fechaComienzo: Timestamp.fromDate(state.fecha),
                              personaElegida1: controller0.text,
                              personaElegida2: controller1.text,
                              personaElegida3: controller2.text,
                              personaElegidaObscure1: state.isObscure0,
                              personaElegidaObscure2: state.isObscure1,
                              personaElegidaObscure3: state.isObscure2,
                              oracionActual: 0,
                            );
                            Navigator.of(context).pushReplacement(
                                MaterialPageRoute<PageOraciones>(
                              builder: (BuildContext context) {
                                return const PageOraciones();
                              },
                            ));
                          },
                        ),
                      );
                    },
                    child: Text(
                      'Continuar',
                      style: TextStyle(
                        fontSize: 17.pf,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}

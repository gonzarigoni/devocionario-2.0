// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:auto_route/auto_route.dart';
import 'package:devocionario2/pages/2-seleccionador_devocion/page_seleccionador_devocion.dart';
import 'package:devocionario2/pages/3-acceso_a_las_promesas/bloc/bloc_acceso_a_las_promesas.dart';
import 'package:devocionario2/pages/3-acceso_a_las_promesas/bloc/bloc_acceso_a_las_promesas_state.dart';
import 'package:devocionario2/services/setters.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:full_responsive/full_responsive.dart';
import 'package:devocionario2/constantes.dart';
import 'package:devocionario2/pages/3-acceso_a_las_promesas/widgets/botones_bottom.dart';
import 'package:devocionario2/pages/3-acceso_a_las_promesas/widgets/container_acceso_promesas.dart';
import 'package:devocionario2/pages/3-acceso_a_las_promesas/widgets/header.dart';
import 'package:devocionario2/pages/3-acceso_a_las_promesas/widgets/seleccionador_fecha.dart';

@RoutePage()
class PageAccesoPromesas extends StatefulWidget {
  const PageAccesoPromesas({
    Key? key,
  }) : super(key: key);

  @override
  State<PageAccesoPromesas> createState() => _PageAccesoPromesasState();
}

class _PageAccesoPromesasState extends State<PageAccesoPromesas> {
  TextEditingController controller0 = TextEditingController();

  TextEditingController controller1 = TextEditingController();

  TextEditingController controller2 = TextEditingController();
  @override
  void initState() {
    updatePaginaActual(newValue: 3);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<BlocAccesoPromesas, BlocAccesoPromesasState>(
      builder: (context, state) {
        return Scaffold(
          backgroundColor: marronclarito,
          appBar: AppBar(
            title: Text(
              'Oración',
              style: TextStyle(
                color: marronoscuro,
                fontSize: 23.pf,
                fontWeight: FontWeight.w400,
              ),
            ),
            elevation: 0,
            centerTitle: true,
            toolbarHeight: 40.ph,
            backgroundColor: Colors.transparent,
            leading: GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => const PageSeleccionadorDevocion()),
                );
              },
              child: const Icon(Icons.arrow_back),
            ),
          ),
          body: SizedBox(
            width: MediaQuery.of(context).size.width,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Header(aniosDevocion: state.anios),
                EspacioVertical(height: 5.ph),
                const SeleccionadorFecha(),
                EspacioVertical(height: 10.ph),
                ContainerAccesoPromesas(
                  aniosDevocion: state.anios,
                  controller0: controller0,
                  controller1: controller1,
                  controller2: controller2,
                ),
                EspacioVertical(height: 60.ph),
              ],
            ),
          ),
          bottomSheet: BotonesBottom(
              aniosDevocion: state.anios,
              controller0: controller0,
              controller1: controller1,
              controller2: controller2),
        );
      },
    );
  }
}

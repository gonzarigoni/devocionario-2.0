import 'package:auto_route/annotations.dart';
import 'package:devocionario2/pages/0-login/widgets/boton_iniciar_sesion.dart';
import 'package:devocionario2/pages/0-login/widgets/textfields_y_olvidaste_contrasenia.dart';
import 'package:devocionario2/pages/0-login/widgets/texto_bienvenida_login.dart';
import 'package:devocionario2/pages/0-login/widgets/todavia_no_estas_registrado.dart';
import 'package:flutter/material.dart';

@RoutePage()
class PageLogin extends StatefulWidget {
  const PageLogin({super.key});

  @override
  State<PageLogin> createState() => _PageLoginState();
}

class _PageLoginState extends State<PageLogin> {
  final TextEditingController controllerEmail = TextEditingController();

  final TextEditingController controllerPassword = TextEditingController();
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: SafeArea(
        child: SizedBox(
          width: MediaQuery.of(context).size.width,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              const TextosBienvenida(),
              TextFieldsYOlvidasteContrasenia(
                  controllerEmail: controllerEmail,
                  controllerPassword: controllerPassword),
              BotonIniciarSesion(
                  controllerEmail: controllerEmail,
                  controllerPassword: controllerPassword),
              const TodaviaNoEstasRegistrado(),
            ],
          ),
        ),
      ),
    );
  }
}

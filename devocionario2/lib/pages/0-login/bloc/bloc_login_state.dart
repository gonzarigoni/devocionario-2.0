// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:equatable/equatable.dart';

class BlocLoginState extends Equatable {
  const BlocLoginState({
    required this.isFilled,
    required this.isValidado,
    required this.isObscure,
  });

  final bool isFilled;
  final bool isValidado;
  final bool isObscure;

  @override
  List<Object> get props => [isFilled, isValidado, isObscure];

  BlocLoginState copyWith({
    bool? isFilled,
    bool? isValidado,
    bool? isObscure,
  }) {
    return BlocLoginState(
      isFilled: isFilled ?? this.isFilled,
      isValidado: isValidado ?? this.isValidado,
      isObscure: isObscure ?? this.isObscure,
    );
  }
}

class LoginInitial extends BlocLoginState {
  const LoginInitial(
      {required bool isFilled,
      required bool isValidado,
      required bool isObscure})
      : super(isFilled: isFilled, isValidado: isValidado, isObscure: isObscure);
}

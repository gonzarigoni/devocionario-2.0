import 'dart:async';

import 'package:devocionario2/pages/0-login/bloc/bloc_login_event.dart';
import 'package:devocionario2/pages/0-login/bloc/bloc_login_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class BlocLogin extends Bloc<BlocLoginEvent, BlocLoginState> {
  BlocLogin()
      : super(const LoginInitial(
            isFilled: false, isValidado: false, isObscure: true)) {
    on<BlocLoginCambiarColorBotonEvent>(_cambiarColorBoton);
    on<BlocLoginCambiarObscureEvent>(_cambiarObscure);
  }

  FutureOr<void> _cambiarColorBoton(
      BlocLoginCambiarColorBotonEvent event, Emitter<BlocLoginState> emit) {
    try {
      final bool isFilled = event.isFilled;
      final bool isValidado = event.isValidado;
      emit(
        state.copyWith(isFilled: isFilled, isValidado: isValidado),
      );
    } catch (e) {
      print(e);
    }
  }

  FutureOr<void> _cambiarObscure(
      BlocLoginCambiarObscureEvent event, Emitter<BlocLoginState> emit) {
    try {
      final bool isObscure = event.isObscure;
      emit(
        state.copyWith(isObscure: isObscure),
      );
    } catch (e) {
      print(e);
    }
  }
}

// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:equatable/equatable.dart';

abstract class BlocLoginEvent extends Equatable {
  const BlocLoginEvent();

  @override
  List<Object> get props => [];
}

class BlocLoginCambiarColorBotonEvent extends BlocLoginEvent {
  const BlocLoginCambiarColorBotonEvent({
    required this.isFilled,
    required this.isValidado,
  });
  final bool isFilled;
  final bool isValidado;
}

class BlocLoginCambiarObscureEvent extends BlocLoginEvent {
  const BlocLoginCambiarObscureEvent({
    required this.isObscure,
  });
  final bool isObscure;
}

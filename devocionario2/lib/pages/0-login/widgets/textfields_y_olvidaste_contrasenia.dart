// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:devocionario2/constantes.dart';
import 'package:devocionario2/pages/0-login/bloc/bloc_login.dart';
import 'package:devocionario2/pages/0-login/bloc/bloc_login_event.dart';
import 'package:devocionario2/pages/0-login/bloc/bloc_login_state.dart';
import 'package:devocionario2/pages/10-restart-password/page_restart_password.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:full_responsive/full_responsive.dart';

class TextFieldsYOlvidasteContrasenia extends StatelessWidget {
  const TextFieldsYOlvidasteContrasenia({
    Key? key,
    required this.controllerEmail,
    required this.controllerPassword,
  }) : super(key: key);
  final TextEditingController controllerEmail;
  final TextEditingController controllerPassword;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          width: 300.pw,
          child: TextField(
            onChanged: (text) {
              if (controllerPassword.text.isNotEmpty &&
                  EmailValidator.validate(controllerEmail.text)) {
                context.read<BlocLogin>().add(
                    const BlocLoginCambiarColorBotonEvent(
                        isFilled: true, isValidado: true));
              } else {
                context.read<BlocLogin>().add(
                    const BlocLoginCambiarColorBotonEvent(
                        isFilled: false, isValidado: false));
              }
            },
            style: const TextStyle(
              color: marronmedio,
            ),
            controller: controllerEmail,
            obscureText: false,
            decoration: const InputDecoration(
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(
                  color: fontTextField,
                ),
              ),
              enabledBorder: UnderlineInputBorder(
                borderSide: BorderSide(
                  color: negro,
                ),
              ),
              labelText: 'Ingrese Email:',
              labelStyle: TextStyle(
                color: fontTextField,
              ),
            ),
          ),
        ),
        EspacioVertical(
          height: 20.ph,
        ),
        BlocBuilder<BlocLogin, BlocLoginState>(
          builder: (context, state) {
            return SizedBox(
              width: 300.pw,
              child: TextField(
                onChanged: (text) {
                  if (controllerPassword.text.isNotEmpty &&
                      EmailValidator.validate(controllerEmail.text)) {
                    context.read<BlocLogin>().add(
                        const BlocLoginCambiarColorBotonEvent(
                            isFilled: true, isValidado: true));
                  } else {
                    context.read<BlocLogin>().add(
                        const BlocLoginCambiarColorBotonEvent(
                            isFilled: false, isValidado: false));
                  }
                },
                style: const TextStyle(
                  color: marronmedio,
                ),
                controller: controllerPassword,
                obscureText: state.isObscure,
                decoration: InputDecoration(
                  focusedBorder: const UnderlineInputBorder(
                    borderSide: BorderSide(
                      color: fontTextField,
                    ),
                  ),
                  enabledBorder: const UnderlineInputBorder(
                    borderSide: BorderSide(
                      color: negro,
                    ),
                  ),
                  labelText: 'Ingrese Contraseña:',
                  labelStyle: const TextStyle(
                    color: fontTextField,
                  ),
                  suffixIcon: BlocBuilder<BlocLogin, BlocLoginState>(
                    builder: (context, state) {
                      return IconButton(
                          onPressed: () {
                            context.read<BlocLogin>().add(
                                BlocLoginCambiarObscureEvent(
                                    isObscure: !state.isObscure));
                          },
                          icon: state.isObscure
                              ? const Icon(
                                  Icons.visibility_off_outlined,
                                  color: marronclarito,
                                )
                              : const Icon(
                                  Icons.visibility_outlined,
                                  color: marronclarito,
                                ));
                    },
                  ),
                ),
              ),
            );
          },
        ),
        EspacioVertical(
          height: 10.ph,
        ),
        GestureDetector(
          onTap: () {
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (BuildContext context) {
                  return PageRestartPassword();
                },
              ),
            );
          },
          child: SizedBox(
            width: 300.pw,
            child: Text(
              '¿Olvidaste tu contraseña?',
              style: TextStyle(
                  fontWeight: FontWeight.w400,
                  fontSize: 13.pf,
                  color: marronclarito),
              textAlign: TextAlign.end,
            ),
          ),
        ),
      ],
    );
  }
}

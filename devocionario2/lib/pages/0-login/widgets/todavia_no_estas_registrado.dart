import 'package:devocionario2/constantes.dart';
import 'package:devocionario2/pages/1-register/page_register.dart';
import 'package:flutter/material.dart';
import 'package:full_responsive/full_responsive.dart';

class TodaviaNoEstasRegistrado extends StatelessWidget {
  const TodaviaNoEstasRegistrado({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context).pushReplacement(MaterialPageRoute<PageRegister>(
          builder: (BuildContext context) {
            return const PageRegister();
          },
        ));
      },
      child: SizedBox(
        width: 300.pw,
        child: RichText(
          textAlign: TextAlign.center,
          text: TextSpan(
            style: TextStyle(
              fontSize: 16.pf,
            ),
            children: const [
              TextSpan(
                text: '¿Todavía no estas ',
                style: TextStyle(
                  color: marronclarito,
                  fontWeight: FontWeight.w700,
                ),
              ),
              TextSpan(
                text: 'registrado',
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: marronmedio,
                ),
              ),
              TextSpan(
                text: '?',
                style: TextStyle(
                  color: marronclarito,
                  fontWeight: FontWeight.w700,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

import 'package:devocionario2/constantes.dart';
import 'package:devocionario2/pages/0-login/bloc/bloc_login.dart';
import 'package:devocionario2/pages/0-login/bloc/bloc_login_state.dart';
import 'package:devocionario2/services/auth.dart';
import 'package:devocionario2/widgets_global/dialog_custom.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:full_responsive/full_responsive.dart';

class BotonIniciarSesion extends StatelessWidget {
  const BotonIniciarSesion({
    Key? key,
    required this.controllerEmail,
    required this.controllerPassword,
  }) : super(key: key);
  final TextEditingController controllerEmail;
  final TextEditingController controllerPassword;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<BlocLogin, BlocLoginState>(
      builder: (context, state) {
        return Container(
          width: 175.pw,
          height: 30.ph,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(15),
          ),
          child: ElevatedButton(
            style: ElevatedButton.styleFrom(
              backgroundColor: state.isValidado && state.isFilled
                  ? marronclarito
                  : disabledColor,
            ),
            onPressed: () async {
              if (state.isValidado && state.isFilled) {
                await signInWithEmailAndPassword(
                    email: controllerEmail.text,
                    password: controllerPassword.text);
              } else {
                showDialog(
                  context: context,
                  builder: (context) => ShowAlertCustom(
                    texto: 'Correo electrónico o contraseña incorrectos',
                    onTap: () async {
                      Navigator.of(context).pop();
                    },
                  ),
                );
              }
            },
            child: Text(
              'Iniciar Sesión',
              style: TextStyle(
                fontWeight: FontWeight.w700,
                fontSize: 17.pf,
              ),
            ),
          ),
        );
      },
    );
  }
}

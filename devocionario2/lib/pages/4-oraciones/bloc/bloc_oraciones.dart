import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:devocionario2/pages/4-oraciones/bloc/bloc_oraciones_event.dart';
import 'package:devocionario2/pages/4-oraciones/bloc/bloc_oraciones_state.dart';
import 'package:devocionario2/services/getters.dart';
import 'package:devocionario2/services/setters.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

class BlocOraciones extends Bloc<BlocOracionesEvent, BlocOracionesState> {
  BlocOraciones()
      : super(OracionesInitial(
          index: 0,
          aniosDevocion: 0,
          fechaComienzo: DateTime.now(),
          fontSize: 3.25,
        )) {
    on<BlocOracionesIncrementarIndexEvent>(_incrementarIndex);
    on<BlocOracionesDecrementarIndexEvent>(_decrementarIndex);
    on<BlocOracionesCambiarFontEvent>(_cambiarFont);
    on<BlocOracionesGetUserDataEvent>(_getUserData);
  }

  FutureOr<void> _incrementarIndex(BlocOracionesIncrementarIndexEvent event,
      Emitter<BlocOracionesState> emit) {
    int limite = event.aniosDevocion == 1 ? 16 : 7;
    final int index =
        event.index! + 1 <= limite ? event.index! + 1 : event.index!;
    updateDevocion(campo: 'oracionActual', newValue: index);
    emit(
      state.copyWith(index: index),
    );
  }

  FutureOr<void> _decrementarIndex(BlocOracionesDecrementarIndexEvent event,
      Emitter<BlocOracionesState> emit) {
    final int index = event.index! - 1 < 0 ? event.index! : event.index! - 1;
    updateDevocion(campo: 'oracionActual', newValue: index);
    emit(
      state.copyWith(index: index),
    );
  }

  FutureOr<void> _cambiarFont(
      BlocOracionesCambiarFontEvent event, Emitter<BlocOracionesState> emit) {
    final double? fontSize = event.fontSize;
    emit(
      state.copyWith(fontSize: fontSize),
    );
  }

  FutureOr<void> _getUserData(BlocOracionesGetUserDataEvent event,
      Emitter<BlocOracionesState> emit) async {
    emit(state.copyWith(status: BlocOracionesStatus.loading));
    try {
      Map<String, dynamic> response = await getUserDataOraciones();
      final int? index = response['oracionActual'];
      final int? aniosDevocion = response['aniosDevocion'];
      final Timestamp? fechaComienzo = response['fechaComienzo'];
      final double? fontSize = response['fontSize'];

      DateTime dateTime = fechaComienzo!.toDate();

      emit(
        state.copyWith(
            index: index,
            aniosDevocion: aniosDevocion,
            fechaComienzo: dateTime.toLocal(),
            fontSize: fontSize,
            status: BlocOracionesStatus.success),
      );
    } catch (e) {
      emit(state.copyWith(
        status: BlocOracionesStatus.error,
      ));
    }
  }
}

String devolverFecha(Timestamp timestamp) {
  DateTime dateTime = timestamp.toDate();
  if (dateTime.day == DateTime.now().day) {
    return '${dateTime.hour.toString().padLeft(2, '0')}:${dateTime.minute.toString().padLeft(2, '0')}';
  } else if (dateTime.day ==
      DateTime.now().subtract(const Duration(days: 1)).day) {
    return 'Ayer';
  } else {
    return '${dateTime.day}/${dateTime.month}/${dateTime.year}';
  }
}

String formatTimestamp(Timestamp timestamp) {
  DateTime dateTime = timestamp.toDate();
  String formattedDate = DateFormat('dd/MM/yy').format(dateTime);
  String formattedTime = DateFormat('HH:mm').format(dateTime);
  return '$formattedDate $formattedTime';
}

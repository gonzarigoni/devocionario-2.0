// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:equatable/equatable.dart';

enum BlocOracionesStatus {
  /// estado inicial sin nada cargado
  initial,

  /// estado cargado
  success,

  /// estado de error
  error,

  /// estado cargando...
  loading;

  ///devuleve true si el estado es Success
  bool get isSuccess => this == BlocOracionesStatus.success;

  ///devuelve true si el estado es Error
  bool get isError => this == BlocOracionesStatus.error;

  ///devuleve true si el estado es Loading
  bool get isLoading => this == BlocOracionesStatus.loading;

  ///devuelve un estado de true al iniciar
  bool get isInitial => this == BlocOracionesStatus.initial;
}

class BlocOracionesState extends Equatable {
  const BlocOracionesState({
    required this.status,
    required this.index,
    required this.aniosDevocion,
    required this.fechaComienzo,
    required this.fontSize,
  });
  final BlocOracionesStatus status;
  final int index;
  final int aniosDevocion;
  final DateTime fechaComienzo;
  final double fontSize;

  @override
  List<Object> get props => [
        status,
        index,
        aniosDevocion,
        fechaComienzo,
        fontSize,
      ];

  BlocOracionesState copyWith({
    int? index,
    final int? aniosDevocion,
    final DateTime? fechaComienzo,
    final double? fontSize,
    final BlocOracionesStatus? status,
  }) {
    return BlocOracionesState(
      aniosDevocion: aniosDevocion ?? this.aniosDevocion,
      fechaComienzo: fechaComienzo ?? this.fechaComienzo,
      index: index ?? this.index,
      fontSize: fontSize ?? this.fontSize,
      status: status ?? this.status,
    );
  }
}

class OracionesInitial extends BlocOracionesState {
  const OracionesInitial({
    required int index,
    required int aniosDevocion,
    required DateTime fechaComienzo,
    required double fontSize,
  }) : super(
          index: index,
          aniosDevocion: aniosDevocion,
          fechaComienzo: fechaComienzo,
          fontSize: fontSize,
          status: BlocOracionesStatus.initial,
        );
}

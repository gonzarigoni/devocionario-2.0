// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:equatable/equatable.dart';

abstract class BlocOracionesEvent extends Equatable {
  const BlocOracionesEvent();

  @override
  List<Object> get props => [];
}

class BlocOracionesIncrementarIndexEvent extends BlocOracionesEvent {
  const BlocOracionesIncrementarIndexEvent({
    required this.index,
    required this.aniosDevocion,
  });
  final int? index;
  final int? aniosDevocion;
}

class BlocOracionesDecrementarIndexEvent extends BlocOracionesEvent {
  const BlocOracionesDecrementarIndexEvent({
    required this.index,
  });
  final int? index;
}

class BlocOracionesCambiarFontEvent extends BlocOracionesEvent {
  const BlocOracionesCambiarFontEvent({
    required this.fontSize,
  });
  final double? fontSize;
}

class BlocOracionesGetUserDataEvent extends BlocOracionesEvent {
  const BlocOracionesGetUserDataEvent({
    required this.index,
    required this.aniosDevocion,
    required this.fechaComienzo,
    required this.fontSize,
  });
  final int? index;
  final int? aniosDevocion;
  final DateTime? fechaComienzo;
  final double? fontSize;
}

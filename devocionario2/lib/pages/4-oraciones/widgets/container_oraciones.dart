// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:devocionario2/pages/4-oraciones/bloc/bloc_oraciones.dart';
import 'package:devocionario2/pages/4-oraciones/bloc/bloc_oraciones_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:devocionario2/constantes.dart';
import 'package:full_responsive/full_responsive.dart';

class ContainerOraciones extends StatelessWidget {
  const ContainerOraciones({
    Key? key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<BlocOraciones, BlocOracionesState>(
      builder: (context, state) {
        return Expanded(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: ClipRRect(
              borderRadius: const BorderRadius.all(
                Radius.circular(20),
              ),
              child: Container(
                color: fondoOraciones,
                child: Padding(
                  padding: const EdgeInsets.all(8),
                  child: SingleChildScrollView(
                    physics: const BouncingScrollPhysics(),
                    child: Padding(
                      padding: const EdgeInsets.all(18),
                      child: Column(
                        children: [
                          state.aniosDevocion == 1
                              ? Titulos(
                                  objTitulos: objTitulos1,
                                )
                              : Titulos(objTitulos: objTitulos12),
                          Padding(
                            padding: const EdgeInsets.all(8),
                            child: Text(
                              '${state.index}/${state.aniosDevocion == 1 ? '16' : '7'}',
                              style: const TextStyle(color: marronmedio),
                            ),
                          ),
                          if (state.index > 0 && state.index < 16)
                            const Padding(
                              padding: EdgeInsets.all(8),
                              child: Text(
                                'Padre Nuestro, Ave María y Gloria',
                                style: TextStyle(
                                  color: blanquito,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 15,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          if (state.index == 0 && state.aniosDevocion == 1)
                            const Padding(
                              padding: EdgeInsets.all(8),
                              child: Text(
                                'Señal de la cruz',
                                style: TextStyle(
                                  color: blanquito,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          state.aniosDevocion == 1
                              ? Oraciones(
                                  objOraciones: objOraciones1,
                                )
                              : Oraciones(
                                  objOraciones: objOraciones12,
                                ),
                          if (state.index == 0 && state.aniosDevocion == 1)
                            const Padding(
                              padding: EdgeInsets.all(8),
                              child: Text(
                                'Credo',
                                style: TextStyle(
                                  color: blanquito,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 17,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          if (state.index == 0 && state.aniosDevocion == 1)
                            const Padding(
                              padding: EdgeInsets.all(8),
                              child: Text(
                                'al Sagrado Corazón de Jesús, haciendo un acto de Fe.',
                                style: TextStyle(
                                  color: marronoscuro,
                                  fontSize: 16,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ),
                          if (state.index == 16)
                            const Padding(
                              padding: EdgeInsets.all(8),
                              child: Text(
                                'Sea por siempre \n bendito y alabado Jesús,\nque con Su Sangre no redimió',
                                style: TextStyle(
                                  color: blanquito,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 17,
                                ),
                                textAlign: TextAlign.center,
                              ),
                            ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}

class Titulos extends StatelessWidget {
  const Titulos({super.key, required this.objTitulos});

  final Map<int, String> objTitulos;
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<BlocOraciones, BlocOracionesState>(
      builder: (context, state) {
        return Text(
          objTitulos[state.index]!,
          style: TextStyle(
            color: marronoscuro,
            fontWeight: FontWeight.bold,
            fontSize: 18.pf,
          ),
          textAlign: TextAlign.center,
        );
      },
    );
  }
}

class Oraciones extends StatelessWidget {
  const Oraciones({
    super.key,
    required this.objOraciones,
  });

  final Map<int, String> objOraciones;
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<BlocOraciones, BlocOracionesState>(
      builder: (context, state) {
        return Text(
          objOraciones[state.index]!,
          style: TextStyle(
            color: marronoscuro,
            fontSize: 5.pf * state.fontSize.pf,
          ),
          textAlign: TextAlign.center,
        );
      },
    );
  }
}

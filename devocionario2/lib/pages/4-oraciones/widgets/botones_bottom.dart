import 'package:devocionario2/constantes.dart';
import 'package:devocionario2/pages/4-oraciones/bloc/bloc_oraciones.dart';
import 'package:devocionario2/pages/4-oraciones/bloc/bloc_oraciones_event.dart';
import 'package:devocionario2/pages/4-oraciones/bloc/bloc_oraciones_state.dart';
import 'package:devocionario2/pages/5-finalizacion/page_finalizacion.dart';
import 'package:devocionario2/services/setters.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:full_responsive/full_responsive.dart';

class BotonesBottom extends StatefulWidget {
  const BotonesBottom({
    super.key,
  });

  @override
  State<BotonesBottom> createState() => _BotonesBottomState();
}

class _BotonesBottomState extends State<BotonesBottom> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<BlocOraciones, BlocOracionesState>(
      builder: (context, state) {
        return Container(
          color: blanquito,
          height: 100.ph,
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'A',
                    style: TextStyle(
                        color: marronmedio,
                        fontSize: 16.pf,
                        fontWeight: FontWeight.w700),
                  ),
                  SizedBox(
                    width: 320.pw,
                    child: Slider(
                      value: state.fontSize,
                      activeColor: marronmedio,
                      inactiveColor: marronmedio,
                      onChanged: (double s) {
                        context
                            .read<BlocOraciones>()
                            .add(BlocOracionesCambiarFontEvent(fontSize: s));
                      },
                      onChangeEnd: (value) => updateFont(newValue: value),
                      divisions: 10,
                      min: 2,
                      max: 4.5,
                    ),
                  ),
                  Text(
                    'A',
                    style: TextStyle(
                        color: marronmedio,
                        fontSize: 24.pf,
                        fontWeight: FontWeight.w700),
                  ),
                ],
              ),
              Center(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    SizedBox(
                      height: 30.ph,
                      width: 120.pw,
                      child: OutlinedButton(
                        style: OutlinedButton.styleFrom(
                          side: BorderSide(
                            width: 2,
                            color: state.index == 0
                                ? disabledColor
                                : marronclarito,
                          ),
                        ),
                        onPressed: () {
                          context.read<BlocOraciones>().add(
                              BlocOracionesDecrementarIndexEvent(
                                  index: state.index));
                        },
                        child: Text(
                          'Atrás',
                          style: TextStyle(
                            color: state.index == 0
                                ? disabledColor
                                : marronclarito,
                            fontSize: 17.pf,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 30.ph,
                      width: 120.pw,
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          backgroundColor: enabledColor,
                        ),
                        onPressed: () async {
                          context.read<BlocOraciones>().add(
                              BlocOracionesIncrementarIndexEvent(
                                  index: state.index,
                                  aniosDevocion: state.aniosDevocion));
                          if (state.aniosDevocion == 1) {
                            if (state.index == 16) {
                              Navigator.of(context).pushReplacement(
                                  MaterialPageRoute<PageFinalizacion>(
                                builder: (BuildContext context) {
                                  return const PageFinalizacion();
                                },
                              ));
                            }
                          } else if (state.index == 7) {
                            Navigator.of(context).pushReplacement(
                                MaterialPageRoute<PageFinalizacion>(
                              builder: (BuildContext context) {
                                return const PageFinalizacion();
                              },
                            ));
                          }
                        },
                        child: Text(
                          'Continuar',
                          style: TextStyle(
                            fontSize: 17.pf,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        );
      },
    );
  }
}

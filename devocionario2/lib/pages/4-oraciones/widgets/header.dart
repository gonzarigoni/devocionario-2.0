// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:devocionario2/pages/4-oraciones/bloc/bloc_oraciones.dart';
import 'package:devocionario2/pages/4-oraciones/bloc/bloc_oraciones_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:full_responsive/full_responsive.dart';

import 'package:devocionario2/constantes.dart';

class Header extends StatelessWidget {
  const Header({
    Key? key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        BlocBuilder<BlocOraciones, BlocOracionesState>(
          builder: (context, state) {
            return Text(
              'Sta. Brígida de ${state.aniosDevocion} ${state.aniosDevocion == 1 ? 'año' : 'años'}',
              style: TextStyle(
                color: marronoscuro,
                fontSize: 23.pf,
                fontWeight: FontWeight.w700,
              ),
            );
          },
        ),
      ],
    );
  }
}

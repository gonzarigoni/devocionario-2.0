import 'package:auto_route/auto_route.dart';
import 'package:devocionario2/constantes.dart';
import 'package:devocionario2/pages/4-oraciones/bloc/bloc_oraciones_event.dart';
import 'package:devocionario2/pages/4-oraciones/bloc/bloc_oraciones_state.dart';
import 'package:devocionario2/services/setters.dart';
import 'package:devocionario2/widgets_global/appbar_custom.dart';
import 'package:devocionario2/widgets_global/dialog_custom.dart';
import 'package:devocionario2/widgets_global/drawer_custom.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:full_responsive/full_responsive.dart';
import 'package:devocionario2/pages/4-oraciones/bloc/bloc_oraciones.dart';
import 'package:devocionario2/pages/4-oraciones/widgets/botones_bottom.dart';
import 'package:devocionario2/pages/4-oraciones/widgets/header.dart';
import 'package:devocionario2/pages/4-oraciones/widgets/container_oraciones.dart';

@RoutePage()
class PageOraciones extends StatefulWidget {
  const PageOraciones({
    Key? key,
  }) : super(key: key);

  @override
  State<PageOraciones> createState() => _PageOracionesState();
}

class _PageOracionesState extends State<PageOraciones> {
  @override
  void initState() {
    updatePaginaActual(newValue: 4);
    context.read<BlocOraciones>().add(BlocOracionesGetUserDataEvent(
          aniosDevocion: 0,
          fechaComienzo: DateTime.now(),
          index: 0,
          fontSize: 0.0,
        ));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: BlocBuilder<BlocOraciones, BlocOracionesState>(
        builder: (context, state) {
          if (state.status == BlocOracionesStatus.loading) {
            return const Scaffold(
              body: Center(
                child: CircularProgressIndicator(
                  color: marronmedio,
                  backgroundColor: Color(0xffdddddd),
                ),
              ),
            );
          }
          if (state.status == BlocOracionesStatus.success) {
            return Scaffold(
              appBar: const AppBarCustom(),
              drawer: const DrawerCustom(),
              body: BlocBuilder<BlocOraciones, BlocOracionesState>(
                builder: (context, state) {
                  return SizedBox(
                    width: MediaQuery.of(context).size.width,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const Header(),
                        EspacioVertical(height: 8.ph),
                        Text(
                          '${state.fechaComienzo.day}/${state.fechaComienzo.month}/${state.fechaComienzo.year}',
                          style: TextStyle(
                            color: marronmedio,
                            fontSize: 16.pf,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                        EspacioVertical(height: 12.ph),
                        SizedBox(
                          width: 300.pw,
                          height: 40.ph,
                          child: ProgressBarView(
                            value: state.index *
                                (state.aniosDevocion == 1 ? 0.058 : 0.125),
                          ),
                        ),
                        EspacioVertical(height: 10.ph),
                        const ContainerOraciones(),
                        EspacioVertical(height: 100.ph),
                      ],
                    ),
                  );
                },
              ),
              bottomSheet: const BotonesBottom(),
            );
          }
          if (state.status == BlocOracionesStatus.error) {
            showDialog(
              context: context,
              builder: (context) => ShowAlertCustom(
                texto: 'Error al obtener los datos del usuario',
                onTap: () async {
                  Navigator.of(context).pop();
                },
              ),
            );
          }
          return Container();
        },
      ),
    );
  }
}

class ProgressBarView extends StatefulWidget {
  const ProgressBarView({super.key, required this.value});
  final double value;
  static const _iconSize = 30;

  @override
  State<ProgressBarView> createState() => _ProgressBarViewState();
}

class _ProgressBarViewState extends State<ProgressBarView> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Padding(
          padding: const EdgeInsets.all(16),
          child: ClipRRect(
            borderRadius: const BorderRadius.all(Radius.circular(10)),
            child: LinearProgressIndicator(
              value: widget.value,
              valueColor: const AlwaysStoppedAnimation<Color>(marronclarito),
              backgroundColor: const Color(0xffD9D9D9),
              minHeight: 6,
            ),
          ),
        ),
        LayoutBuilder(
          builder: (context, constrains) {
            // ignore: omit_local_variable_types
            double leftPadding = 0;
            if (widget.value < .3 && widget.value != 0.0) {
              leftPadding = constrains.maxWidth * widget.value -
                  ProgressBarView._iconSize +
                  20;
            }
            if (widget.value >= .3) {
              leftPadding = constrains.maxWidth * widget.value -
                  ProgressBarView._iconSize +
                  10;
            }
            final topPadding =
                (constrains.maxHeight - ProgressBarView._iconSize) / 2;
            return Padding(
              padding: EdgeInsets.only(left: leftPadding, top: topPadding),
              child: Transform.translate(
                offset: const Offset(0, -5),
                child: FaIcon(
                  FontAwesomeIcons.bookBible,
                  size: ProgressBarView._iconSize.toDouble(),
                  color: marronclarito,
                ),
              ),
            );
          },
        )
      ],
    );
  }
}

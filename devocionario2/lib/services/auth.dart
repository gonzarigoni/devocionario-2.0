// ignore_for_file: avoid_print

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

class Auth {
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  User? get currentUser => _firebaseAuth.currentUser;

  Stream<User?> get authStateChanges => _firebaseAuth.authStateChanges();

  Future<void> signInWithEmailAndPassword(
      {required String email, required String password}) async {
    await _firebaseAuth.signInWithEmailAndPassword(
        email: email, password: password);
  }

  Future<void> createUserWithEmailAndPassword(
      {required String email, required String password}) async {
    await _firebaseAuth.createUserWithEmailAndPassword(
        email: email, password: password);
  }

  Future<void> signOut() async {
    await _firebaseAuth.signOut();
  }

  // getProfilePicture() async {
  //   try {
  //     if (_firebaseAuth.currentUser!.photoURL != null) {
  //       return Image.network(
  //         _firebaseAuth.currentUser!.photoURL!,
  //         height: 100,
  //         width: 100,
  //       );
  //     }
  //   } catch (e) {
  //     // Error retrieving the document
  //     print('Error retrieving document: $e');
  //   }
  //   return;
  // }
}

Future<void> signOut() async {
  await Auth().signOut();
}

Future<void> signInWithEmailAndPassword(
    {required String email, required String password}) async {
  try {
    Auth().signInWithEmailAndPassword(email: email, password: password);
  } on FirebaseAuthException catch (e) {
    print(e);
  }
}

Future registerUserWithEmailAndPassword(
  String email,
  String password,
) async {
  try {
    UserCredential userCredential =
        await FirebaseAuth.instance.createUserWithEmailAndPassword(
      email: email,
      password: password,
    );
    return userCredential;
  } catch (e) {
    // Handle registration errors
    print('Error registering user: $e');
    return null;
  }
}

Future<void> storeUserDetails(
  String uid,
  String name,
  String lastname,
  String email,
) async {
  try {
    await FirebaseFirestore.instance.collection('usuarios').doc(uid).set({
      'name': name,
      'lastname': lastname,
      'email': email,
      'fontSize': 3.25,
    });
    await FirebaseFirestore.instance.collection('devociones').doc(uid).set({
      'devocion': 0,
      'fechaComienzo': Timestamp.now(),
      'personaElegida1': '',
      'personaElegida2': '',
      'personaElegida3': '',
      'personaElegidaObscure1': false,
      'personaElegidaObscure2': false,
      'personaElegidaObscure3': false,
      'paginaActual': 2,
      'oracionActual': 0,
      'diasCumplidos': 0,
      'ultimoDiaCumplido': '19000101',
    });
  } catch (e) {
    // Handle storing user details error
    print('Error storing user details: $e');
  }
}

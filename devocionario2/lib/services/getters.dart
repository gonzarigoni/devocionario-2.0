// ignore_for_file: avoid_print

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

Future<dynamic> getAllFielsdValue(String collection, String documentId) async {
  try {
    DocumentSnapshot snapshot = await FirebaseFirestore.instance
        .collection(collection)
        .doc(documentId)
        .get();

    if (snapshot.exists) {
      // Retrieve the value of the specified field
      dynamic fieldValue = snapshot.data();
      return fieldValue;
    } else {
      // Document does not exist
      print('Document does not exist');
      return null;
    }
  } catch (e) {
    // Error retrieving the document
    print('Error retrieving document: $e');
    return null;
  }
}

Future<dynamic> getFieldValue(
    {required String colection, required String campo}) async {
  try {
    var id = FirebaseAuth.instance.currentUser!.uid;
    DocumentSnapshot snapshot =
        await FirebaseFirestore.instance.collection(colection).doc(id).get();

    if (snapshot.exists) {
      // Retrieve the value of the specified field
      dynamic fieldValue = snapshot.data();
      return fieldValue[campo];
    } else {
      // Document does not exist
      print('Document does not exist');
      return null;
    }
  } catch (e) {
    // Error retrieving the document
    print('Error retrieving document: $e');
    return null;
  }
}

Future<Map<String, dynamic>> getUserDataOraciones() async {
  try {
    var id = FirebaseAuth.instance.currentUser!.uid;

    var responseDevociones = await getAllFielsdValue('devociones', id);
    var responseUsuarios = await getAllFielsdValue('usuarios', id);

    Map<String, dynamic> respuesta = {
      'aniosDevocion': responseDevociones['devocion'],
      'fechaComienzo': responseDevociones['fechaComienzo'],
      'oracionActual': responseDevociones['oracionActual'],
      'fontSize': responseUsuarios['fontSize']
    };
    return respuesta;
  } catch (e) {
    // Error retrieving the document
    print('Error retrieving document: $e');
  }
  return {};
}

Future<Map<String, dynamic>> getUserDataPromesas() async {
  try {
    var id = FirebaseAuth.instance.currentUser!.uid;

    var responseDevociones = await getAllFielsdValue('devociones', id);

    Map<String, dynamic> respuesta = {
      'aniosDevocion': responseDevociones['devocion'],
      'fechaComienzo': responseDevociones['fechaComienzo'],
      'ultimoDiaCumplido': responseDevociones['ultimoDiaCumplido'],
      'personaElegida1': responseDevociones['personaElegida1'],
      'personaElegida2': responseDevociones['personaElegida2'],
      'personaElegida3': responseDevociones['personaElegida3'],
      'personaElegidaObscure1': responseDevociones['personaElegidaObscure1'],
      'personaElegidaObscure2': responseDevociones['personaElegidaObscure2'],
      'personaElegidaObscure3': responseDevociones['personaElegidaObscure3'],
    };
    return respuesta;
  } catch (e) {
    // Error retrieving the document
    print('Error retrieving document: $e');
  }
  return {};
}

Future<Map<String, dynamic>> getUserDataProgreso() async {
  try {
    var id = FirebaseAuth.instance.currentUser!.uid;

    var responseDevociones = await getAllFielsdValue('devociones', id);

    Map<String, dynamic> respuesta = {
      'aniosDevocion': responseDevociones['devocion'],
      'diasCumplidos': responseDevociones['diasCumplidos'],
      'ultimoDiaCumplido': responseDevociones['ultimoDiaCumplido'],
    };
    print('respuesta $respuesta');
    return respuesta;
  } catch (e) {
    // Error retrieving the document
    print('Error retrieving document: $e');
  }
  return {};
}

Future<Map<String, dynamic>> getUserDataProfile() async {
  try {
    var id = FirebaseAuth.instance.currentUser!.uid;

    var responseUsuarios = await getAllFielsdValue('usuarios', id);

    Map<String, dynamic> respuesta = {
      'nombre': responseUsuarios['name'],
      'apellido': responseUsuarios['lastname'],
      'email': responseUsuarios['email'],
    };
    return respuesta;
  } catch (e) {
    // Error retrieving the document
    print('Error retrieving document: $e');
  }
  return {};
}

Future<Map<String, dynamic>> getUserDataDrawer() async {
  try {
    var id = FirebaseAuth.instance.currentUser!.uid;

    var responseDevociones = await getAllFielsdValue('usuarios', id);

    Map<String, dynamic> respuesta = {
      'name': responseDevociones['name'],
      'lastname': responseDevociones['lastname'],
    };
    return respuesta;
  } catch (e) {
    // Error retrieving the document
    print('Error retrieving document: $e');
  }
  return {};
}

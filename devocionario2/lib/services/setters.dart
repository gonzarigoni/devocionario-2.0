// ignore_for_file: avoid_print

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:devocionario2/services/getters.dart';
import 'package:firebase_auth/firebase_auth.dart';

FirebaseFirestore db = FirebaseFirestore.instance;

Future<void> updatePaginaActual({required var newValue}) async {
  var id = FirebaseAuth.instance.currentUser!.uid;
  DocumentReference docRef = db.collection('devociones').doc(id);

// Update a specific field in the document
  try {
    docRef.update({
      'paginaActual': newValue,
    }).then((_) {
      print('paginaActual updated successfully!');
    });
  } catch (e) {
    return print(e);
  }
}

Future<void> updateOracionActual({required var newValue}) async {
  var id = FirebaseAuth.instance.currentUser!.uid;
  DocumentReference docRef = db.collection('devociones').doc(id);

// Update a specific field in the document
  try {
    docRef.update({
      'oracionActual': newValue,
    }).then((_) {
      print('oracionActual updated successfully to $newValue!');
    });
  } catch (e) {
    return print(e);
  }
}

Future<void> updateDiasCumplidos() async {
  var id = FirebaseAuth.instance.currentUser!.uid;
  DocumentReference docRef = db.collection('devociones').doc(id);

  var response = await getAllFielsdValue('devociones', id);

// Update a specific field in the document
  try {
    docRef.update({
      'diasCumplidos': response['diasCumplidos'] + 1,
    }).then((_) {
      print('diasCumplidos updated successfully!');
    });
  } catch (e) {
    return print(e);
  }
}

Future<void> updateFont({required var newValue}) async {
  var id = FirebaseAuth.instance.currentUser!.uid;
  DocumentReference docRef = db.collection('usuarios').doc(id);

// Update a specific field in the document
  try {
    docRef.update({
      'fontSize': newValue,
    }).then((_) {
      print('fontSize updated successfully!');
    });
  } catch (e) {
    return print(e);
  }
}

Future<void> updateUsuario(
    {required String campo, required var newValue}) async {
  var id = FirebaseAuth.instance.currentUser!.uid;
  DocumentReference docRef = db.collection('usuarios').doc(id);

// Update a specific field in the document
  try {
    docRef.update({
      campo: newValue,
    }).then((_) {
      print('$campo updated successfully in usuarios!');
    });
  } catch (e) {
    return print(e);
  }
}

Future<void> updateDevocion(
    {required String campo, required var newValue}) async {
  var id = FirebaseAuth.instance.currentUser!.uid;
  DocumentReference docRef = db.collection('devociones').doc(id);

// Update a specific field in the document
  try {
    docRef.update({
      campo: newValue,
    }).then((_) {
      print('$campo updated successfully in devociones!');
    });
  } catch (e) {
    return print(e);
  }
}

Future<void> updatePaginaAnterior({required var newValue}) async {
  var id = FirebaseAuth.instance.currentUser!.uid;
  DocumentReference docRef = db.collection('devociones').doc(id);

// Update a specific field in the document
  try {
    docRef.update({
      'paginaAnterior': newValue,
    }).then((_) {
      print('paginaAnterior updated successfully!');
    });
  } catch (e) {
    return print(e);
  }
}

Future<void> updateFechaSeleccionada({required var newValue}) async {
  var id = FirebaseAuth.instance.currentUser!.uid;
  DocumentReference docRef = db.collection('devociones').doc(id);

// Update a specific field in the document
  try {
    docRef.update({
      'fechaComienzo': newValue,
    }).then((_) {
      print('fechaComienzo updated successfully!');
    });
  } catch (e) {
    return print(e);
  }
}

Future<void> updateObscures(
    {required bool personaElegidaObscure1,
    required bool personaElegidaObscure2,
    required bool personaElegidaObscure3}) async {
  var id = FirebaseAuth.instance.currentUser!.uid;
  DocumentReference docRef = db.collection('devociones').doc(id);

// Update a specific field in the document
  try {
    docRef.update({
      'personaElegidaObscure1': personaElegidaObscure1,
      'personaElegidaObscure2': personaElegidaObscure2,
      'personaElegidaObscure3': personaElegidaObscure3,
    }).then((_) {
      print('updateObscures successfully!');
    });
  } catch (e) {
    return print(e);
  }
}

Future<void> updateUserSettings({
  required int devocion,
  required int diasCumplidos,
  required Timestamp fechaComienzo,
  required String personaElegida1,
  required String personaElegida2,
  required String personaElegida3,
  required bool personaElegidaObscure1,
  required bool personaElegidaObscure2,
  required bool personaElegidaObscure3,
  required int oracionActual,
}) async {
  var id = FirebaseAuth.instance.currentUser!.uid;
  DocumentReference docRef = db.collection('devociones').doc(id);

// Update a specific field in the document
  try {
    docRef.update({
      'devocion': devocion,
      'diasCumplidos': diasCumplidos,
      'fechaComienzo': fechaComienzo,
      'personaElegida1': personaElegida1,
      'personaElegida2': personaElegida2,
      'personaElegida3': personaElegida3,
      'personaElegidaObscure1': personaElegidaObscure1,
      'personaElegidaObscure2': personaElegidaObscure2,
      'personaElegidaObscure3': personaElegidaObscure3,
      'oracionActual': oracionActual,
    }).then((_) {
      print('updateUserSettings successfully!');
    });
  } catch (e) {
    return print(e);
  }
}

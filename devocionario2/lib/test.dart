// ignore_for_file: avoid_print

import 'package:intl/intl.dart';

void main() {
  String lafecha = '2023-06-03 03:24:17.651';
  var fecha = '';
  DateTime now = DateTime.parse(lafecha);
// DateTime now = DateTime.now();
  String hora = DateFormat('HH').format(now);
  if (int.parse(hora) < 5) {
    fecha =
        DateFormat('yyyyMMdd').format(now.subtract(const Duration(days: 1)));
  } else {
    fecha = DateFormat('yyyyMMdd').format(now);
  }
// response['ultimoDiaCumplido']

  print(fecha);
}
